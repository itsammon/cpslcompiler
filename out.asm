.globl main
.text
main: la $gp, GA
	ori $fp, $sp, 0
	 j prog

# Function getArray
F1: #Allocate space for variables
	addi $sp, $sp, -88
# Begining of for loop
# Set aside space for the integer variable
	addi $sp, $sp, -4
# Moving the constant into a register
	li $t9, 0
	addi $t8, $fp, -96
	sw $t9, 0($t8)
L0: # Moving the constant into a register
	li $t9, 11
# Load an lval
	addi $t8, $fp, -96
	lw $t7, 0($t8)
	beq $t7, $t9, L1
# Load an lval
	addi $t8, $fp, -96
	lw $t6, 0($t8)
	li $t8, 2
	mult $t8, $t6
	mflo $t8
	addi $t6, $fp, -132
# Load an lval
	addi $t5, $fp, -96
	lw $t4, 0($t5)
	subi $t5, $t4, 0
	li $t4, 4
	mult $t4, $t5
	mflo $t4
	add $t5,$t6,$t4
# Store the value in the appropriate address
	sw $t8, 0($t5)
# Increment variable 
	addi $t7, $t7, 1
	addi $t8, $fp, -96
	sw $t7, 0($t8)
	j L0
# Restore the stack to its old value
	addi $sp, $sp, 4
#End of for loop
L1: 
# Load an lval
	addi $t9, $fp, -132
	addi $t7, $t9, 0
# Copying Structure
	addi $t8, $fp, -88
# Moving memory block
	lw $t9, 0($t7)
	sw $t9, 0($t8)
# Moving memory block
	lw $t9, 4($t7)
	sw $t9, 4($t8)
# Moving memory block
	lw $t9, 8($t7)
	sw $t9, 8($t8)
# Moving memory block
	lw $t9, 12($t7)
	sw $t9, 12($t8)
# Moving memory block
	lw $t9, 16($t7)
	sw $t9, 16($t8)
# Moving memory block
	lw $t9, 20($t7)
	sw $t9, 20($t8)
# Moving memory block
	lw $t9, 24($t7)
	sw $t9, 24($t8)
# Moving memory block
	lw $t9, 28($t7)
	sw $t9, 28($t8)
# Moving memory block
	lw $t9, 32($t7)
	sw $t9, 32($t8)
# Moving memory block
	lw $t9, 36($t7)
	sw $t9, 36($t8)
# Moving memory block
	lw $t9, 40($t7)
	sw $t9, 40($t8)
# Return statement
	 j F1_epi
# epilogue
F1_epi: addi $sp, $sp, 88
	jr $ra

prog: 
# Begining of for loop
# Set aside space for the integer variable
	addi $sp, $sp, -4
# Moving the constant into a register
	li $t9, 0
	addi $t7, $fp, -8
	sw $t9, 0($t7)
L2: # Moving the constant into a register
	li $t9, 11
# Load an lval
	addi $t7, $fp, -8
	lw $t8, 0($t7)
	beq $t8, $t9, L3
# writing a string to the screen
	li $v0, 4
	la $a0, SL0
	syscall
# Load an lval
	addi $t7, $fp, -8
	lw $t5, 0($t7)
# writing a int from the screen
	li $v0, 1
	move $a0, $t5
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL1
	syscall
# Load an lval
	addi $t5, $fp, -8
	lw $t7, 0($t5)
	li $t5, 5
	mult $t5, $t7
	mflo $t5
	addi $t7, $gp, 0
# Load an lval
	addi $t4, $fp, -8
	lw $t6, 0($t4)
	subi $t4, $t6, 0
	li $t6, 4
	mult $t6, $t4
	mflo $t6
	add $t4,$t7,$t6
# Store the value in the appropriate address
	sw $t5, 0($t4)
# Load an lval
	addi $t5, $fp, -8
	lw $t4, 0($t5)
	li $t5, 3
	mult $t5, $t4
	mflo $t5
	addi $t4, $gp, 44
# Load an lval
	addi $t6, $fp, -8
	lw $t7, 0($t6)
	subi $t6, $t7, 0
	li $t7, 4
	mult $t7, $t6
	mflo $t7
	add $t6,$t4,$t7
# Store the value in the appropriate address
	sw $t5, 0($t6)
# Increment variable 
	addi $t8, $t8, 1
	addi $t5, $fp, -8
	sw $t8, 0($t5)
	j L2
# Restore the stack to its old value
	addi $sp, $sp, 4
#End of for loop
L3: 
# Begining of for loop
# Set aside space for the integer variable
	addi $sp, $sp, -4
# Moving the constant into a register
	li $t9, 0
	addi $t8, $fp, -8
	sw $t9, 0($t8)
L4: # Moving the constant into a register
	li $t9, 11
# Load an lval
	addi $t8, $fp, -8
	lw $t5, 0($t8)
	beq $t5, $t9, L5
# writing a string to the screen
	li $v0, 4
	la $a0, SL2
	syscall
# Load an lval
	addi $t8, $fp, -8
	lw $t6, 0($t8)
# writing a int from the screen
	li $v0, 1
	move $a0, $t6
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL3
	syscall
# Load an lval
	addi $t6, $gp, 0
# Load an lval
	addi $t8, $fp, -8
	lw $t7, 0($t8)
	subi $t8, $t7, 0
	li $t7, 4
	mult $t7, $t8
	mflo $t7
	add $t8,$t6,$t7
	lw $t7, 0($t8)
# writing a int from the screen
	li $v0, 1
	move $a0, $t7
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL4
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL5
	syscall
# Load an lval
	addi $t7, $fp, -8
	lw $t8, 0($t7)
# writing a int from the screen
	li $v0, 1
	move $a0, $t8
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL6
	syscall
# Load an lval
	addi $t8, $gp, 44
# Load an lval
	addi $t7, $fp, -8
	lw $t6, 0($t7)
	subi $t7, $t6, 0
	li $t6, 4
	mult $t6, $t7
	mflo $t6
	add $t7,$t8,$t6
	lw $t6, 0($t7)
# writing a int from the screen
	li $v0, 1
	move $a0, $t6
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL7
	syscall
# Increment variable 
	addi $t5, $t5, 1
	addi $t6, $fp, -8
	sw $t5, 0($t6)
	j L4
# Restore the stack to its old value
	addi $sp, $sp, 4
#End of for loop
L5: 
# Function Call to F1
	addi $sp, $sp, -8
# Spill Registers
	sw $ra, 0($sp)
	sw $fp, 4($sp)
	move $fp, $sp
	jal F1
	addi $t9, $fp, -88
	move $v1, $t9
# Move sp below arguements
	addi $sp, $sp, 0
# Restore Registers
	lw $ra, 0($sp)
	lw $fp, 4($sp)
# Restore sp
	addi $sp, $sp, 8
# Get the value into a normal register
	move $t9, $v1
# Copying Structure
	addi $t6, $gp, 0
# Moving memory block
	lw $t5, 0($t9)
	sw $t5, 0($t6)
# Moving memory block
	lw $t5, 4($t9)
	sw $t5, 4($t6)
# Moving memory block
	lw $t5, 8($t9)
	sw $t5, 8($t6)
# Moving memory block
	lw $t5, 12($t9)
	sw $t5, 12($t6)
# Moving memory block
	lw $t5, 16($t9)
	sw $t5, 16($t6)
# Moving memory block
	lw $t5, 20($t9)
	sw $t5, 20($t6)
# Moving memory block
	lw $t5, 24($t9)
	sw $t5, 24($t6)
# Moving memory block
	lw $t5, 28($t9)
	sw $t5, 28($t6)
# Moving memory block
	lw $t5, 32($t9)
	sw $t5, 32($t6)
# Moving memory block
	lw $t5, 36($t9)
	sw $t5, 36($t6)
# Moving memory block
	lw $t5, 40($t9)
	sw $t5, 40($t6)
# Begining of for loop
# Set aside space for the integer variable
	addi $sp, $sp, -4
# Moving the constant into a register
	li $t5, 0
	addi $t9, $fp, -8
	sw $t5, 0($t9)
L6: # Moving the constant into a register
	li $t5, 11
# Load an lval
	addi $t9, $fp, -8
	lw $t6, 0($t9)
	beq $t6, $t5, L7
# writing a string to the screen
	li $v0, 4
	la $a0, SL8
	syscall
# Load an lval
	addi $t9, $fp, -8
	lw $t7, 0($t9)
# writing a int from the screen
	li $v0, 1
	move $a0, $t7
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL9
	syscall
# Load an lval
	addi $t7, $gp, 0
# Load an lval
	addi $t9, $fp, -8
	lw $t8, 0($t9)
	subi $t9, $t8, 0
	li $t8, 4
	mult $t8, $t9
	mflo $t8
	add $t9,$t7,$t8
	lw $t8, 0($t9)
# writing a int from the screen
	li $v0, 1
	move $a0, $t8
	syscall
# writing a string to the screen
	li $v0, 4
	la $a0, SL10
	syscall
# Increment variable 
	addi $t6, $t6, 1
	addi $t8, $fp, -8
	sw $t6, 0($t8)
	j L6
# Restore the stack to its old value
	addi $sp, $sp, 4
#End of for loop
L7: 
prog_epi: 
#End the program
	li $v0, 10
	syscall
.data
SL0: .asciiz "n:"
SL1: .asciiz "\n"
SL2: .asciiz "a["
SL3: .asciiz "]:"
SL4: .asciiz "\n"
SL5: .asciiz "b["
SL6: .asciiz "]:"
SL7: .asciiz "\n\n"
SL8: .asciiz "a["
SL9: .asciiz "]:"
SL10: .asciiz "\n"
GA: 
.align 2
.space 352
