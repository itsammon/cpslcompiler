#include "ForStatement.hpp"
#include <iostream>
#include "LabelGenerator.hpp"
using namespace std;

namespace Clegg5300
{
void ForStatement::emit()
{
	unique_ptr<Register> reg;
	unique_ptr<Register> reg2;
	// label the top and bottom of the while loop
	string topLabel = LabelGenerator::getLabel();
	string endLabel = LabelGenerator::getLabel();

	// Emit assignment after allocating space for variable
	oFile << "# Begining of for loop" << endl;
	head->initialize();

	// Output the top of the loop and the reload value
	oFile << topLabel << ": ";
	// Output the condition check
	reg2 = to->expression();
	reg = head->loadValue();

	// Output the condition check
	oFile << "\tbeq " << *reg << ", " << *reg2 << ", " << endLabel << endl;

	// Emit the statement sequence
	if (sequence != nullptr)
		sequence->emit();

	// Increment the integer value, the value of the register is still in reg
	oFile << "# Increment variable " << endl;
	oFile << "\taddi " << *reg << ", " << *reg << ", " << to->increment() << endl;
	head->storeValue(std::move(reg));

	// Jump to the top
	oFile << "\tj " << topLabel << endl;
	// Restore the stack pointer at the end
	head->restoreStack();
	oFile << "#End of for loop" << endl;
	oFile << endLabel << ": " << endl;
}

}
