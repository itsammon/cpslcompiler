#ifndef ELSE_STATEMENT_HPP
#define ELSE_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include "Statement.hpp"
#include "StatementSequence.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ElseStatement : public Statement
{
	public:
		ElseStatement(std::shared_ptr<StatementSequence> seq) : sequence(seq) {}
		void emit();
	private:
		std::shared_ptr<StatementSequence> sequence;
};

}

#endif
