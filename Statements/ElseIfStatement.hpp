#ifndef ELSE_IF_STATEMENT_HPP
#define ELSE_IF_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include <memory>
#include "StatementSequence.hpp"
#include "ElseStatement.hpp"
#include "Expression.hpp"
#include "LabelGenerator.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ElseIfStatement : public Statement
{
	public:
		ElseIfStatement(std::shared_ptr<Expression> e, std::shared_ptr<StatementSequence> seq) : exp(e), sequence(seq) { eiStatement = nullptr; label = LabelGenerator::getLabel(); }
		// Used to give the correct jump label at the end
		void emit(std::string elseLabel, std::string endLabel);
		// Needed because of inheritance
		void emit() {}
		std::string getLabel() { return label; }
		void addElseIf(std::shared_ptr<ElseIfStatement> e) { eiStatement = e; }
	private:
		std::shared_ptr<StatementSequence> sequence;
		std::shared_ptr<Expression> exp;
		std::shared_ptr<ElseIfStatement> eiStatement;
		std::string label;
};

}

#endif
