#ifndef STOP_STATEMENT_HPP
#define STOP_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include "Statement.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class StopStatement : public Statement
{
	public:
		StopStatement() {};
		void emit();

	private:
		
};

}

#endif
