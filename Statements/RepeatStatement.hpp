#ifndef REPEAT_STATEMENT_HPP
#define REPEAT_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include "Statement.hpp"
#include "Expression.hpp"
#include "StatementSequence.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class RepeatStatement : public Statement
{
	public:
		RepeatStatement(std::shared_ptr<StatementSequence> seq, std::shared_ptr<Expression> e) : exp(e), sequence(seq) {}
		void emit();
	private:
		std::shared_ptr<StatementSequence> sequence;
		std::shared_ptr<Expression> exp;
};

}

#endif
