#ifndef READ_STATEMENT_HPP
#define READ_STATEMENT_HPP

#include <iostream>
#include <fstream>

#include "Statement.hpp"
#include "LVal.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{
class ReadStatement : public Statement
{
	public:
		ReadStatement(shared_ptr<LVal> l);
		void emit();
		void append(shared_ptr<LVal> l);
	private:
		std::vector<shared_ptr<LVal>> lvals;
};



}

#endif
