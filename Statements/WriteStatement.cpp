#include "WriteStatement.hpp"
#include <iostream>
using namespace std;

namespace Clegg5300
{

WriteStatement::WriteStatement(shared_ptr<Expression> e)
{
	exps.push_back(e);
}

void WriteStatement::emit()
{
	for (auto exp : exps)
	{
		exp->emit();
		if (exp->getType() == BuiltInType::getChar())
		{
			oFile << "# writing a char to the screen" << endl;
			oFile << "\tli $v0, 11" << endl;
			if (exp->isConst())
			{
				oFile << "\tli $a0, " << exp->getValue() << endl;
			}
			else
			{
				oFile << "\tmove $a0, " << *(exp->getRegister()) << endl;
			}
			oFile << "\tsyscall" << endl;
		}
		else if (exp->getType() == BuiltInType::getInt())
		{
			oFile << "# writing a int from the screen" << endl;
			oFile << "\tli $v0, 1" << endl;
			if (exp->isConst())
			{
				oFile << "\tli $a0, " << exp->getValue();
			}
			else
			{
				oFile << "\tmove $a0, " << *(exp->getRegister()) << endl;
			}
			oFile << "\tsyscall" << endl;
		}
		else if (exp->getType() == BuiltInType::getString())
		{
			oFile << "# writing a string to the screen" << endl;
			oFile << "\tli $v0, 4" << endl;
			oFile << "\tla $a0, SL" << exp->getValue() << endl;
			oFile << "\tsyscall" << endl;

		}
		else if (exp->getType() == BuiltInType::getBool())
		{
			oFile << "# writing a bool from the screen" << endl;
			oFile << "\tli $v0, 1" << endl;
			if (exp->isConst())
			{
				oFile << "\tli $a0, " << exp->getValue() << endl;
			}
			else
			{
				oFile << "\tmove $a0, " << *(exp->getRegister()) << endl;
			}
			oFile << "\tsyscall" << endl;
		}
	}
}

void WriteStatement::append(shared_ptr<Expression> e)
{
	exps.push_back(e);
}
}
