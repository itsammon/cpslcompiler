#include "ElseIfStatement.hpp"
#include <iostream>
#include "Expression.hpp"
#include "LabelGenerator.hpp"
#include <memory>
using namespace std;

namespace Clegg5300
{
void ElseIfStatement::emit(string elseLabel, string endLabel)
{
	unique_ptr<Register> reg;
	string elseIfLabel;
	
	// Emit our label
	oFile << label << ": ";

	// Set the values to what they need to be for all expressions
	exp->emit();
	
	// If it is constant I need to get a register to store the value
	if (exp->isConst())
	{
		oFile << "# Moving the constant into a register" << endl;
		reg = Register::allocateRegister();
		oFile << "\tli " << *reg << ", " << exp->getValue() << endl;
	}
	else
	{	
		reg = exp->getRegister();
	}
	
	if (eiStatement != nullptr)
	{
		elseIfLabel = eiStatement->getLabel();
	}
	else
	{
		elseIfLabel = elseLabel;
	}

	oFile << "#execute ifelse statement" << endl;
	oFile << "\t beq " << *reg << ", $zero, " << elseIfLabel << endl;
		
	// Emit our sequence
	if (sequence != nullptr)
	{
		oFile << "#ElseIf Statement Sequence" << endl;
		sequence->emit();
		oFile << "#End ElseIf Statement Sequence" << endl;
	}
	oFile << "\t j " << endLabel << endl;

	// If there is an ifelse part, we need to emit it
	if (eiStatement != nullptr)
	{
		eiStatement->emit(elseLabel, endLabel);
	}
}

}
