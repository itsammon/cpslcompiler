#ifndef STATEMENT_HPP
#define STATEMENT_HPP

#include <iostream>
#include <fstream>

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class Statement
{
	public:
		Statement();
		virtual void emit() = 0;
	private:
		
};



}

#endif
