#ifndef TO_HEAD_HPP
#define TO_HEAD_HPP

#include <iostream>
#include <fstream>
#include "Expression.hpp"
#include <memory>
#include "Register.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ToHead
{
	public:
		ToHead(int i, std::shared_ptr<Expression> e) : inc(i), exp(e) {}
		unique_ptr<Register> expression();
		int increment() { return inc; }
	private:
		int inc;
		std::shared_ptr<Expression> exp;
};

}

#endif
