#include "StopStatement.hpp"
#include <iostream>
using namespace std;

namespace Clegg5300
{

void StopStatement::emit()
{
	oFile << "#End the program" << endl;
	oFile << "\tli $v0, 10" << endl;
	oFile << "\tsyscall" << endl;
}

}
