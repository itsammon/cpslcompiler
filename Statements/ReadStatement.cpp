#include "ReadStatement.hpp"
#include <iostream>
using namespace std;

namespace Clegg5300
{
ReadStatement::ReadStatement(shared_ptr<LVal> l)
{
	lvals.push_back(l);
}

void ReadStatement::emit()
{
	for (auto lval : lvals)
	{
		if (lval->getType() == BuiltInType::getChar())
		{
			oFile << "# reading a char from the user" << endl;
			oFile << "\tli $v0, 12" << endl;
			oFile << "\tsyscall" << endl;
			auto address = lval->getAddress();
			address->emit();
			oFile << "# Store the value in the appropriate address" << endl;		
			if (address->isConst())
			{
				auto addr = address->getValue();
				oFile << "\tsw $v0, ";
				if (lval->getMemLocation() == STACK)
					oFile << "-";
				oFile << addr << "(";
				if (lval->getMemLocation() != GLOBAL)
					oFile << "$gp)" << endl;
				else
					oFile << "$fp)" << endl;
			}
			else
			{
				auto addr = address->getRegister();
				oFile << "\tsw $v0, ";
				oFile << "0(" << *addr << ")" << endl;
			}
		}
		else if (lval->getType() == BuiltInType::getInt())
		{
			oFile << "# reading a int from the user" << endl;
			oFile << "\tli $v0, 5" << endl;
			oFile << "\tsyscall" << endl;
			auto address = lval->getAddress();
			address->emit();
			if (address->isConst())
			{
				auto addr = address->getValue();
				oFile << "\tsw $v0, ";
				if (lval->getMemLocation() == STACK)
					oFile << "-";
				oFile << addr << "(";
				if (lval->getMemLocation() != GLOBAL)
					oFile << "$gp)" << endl;
				else
					oFile << "$fp)" << endl;
			}
			else
			{
				auto addr = address->getRegister();
				oFile << "\tsw $v0, ";
				oFile << "0(" << *addr << ")" << endl;
			}
		}
	}
}

void ReadStatement::append(shared_ptr<LVal> l)
{
	lvals.push_back(l);
}

}
