#include "ForHead.hpp"
#include <iostream>
#include "Expression.hpp"
#include "LVal.hpp"
#include "LoadExpression.hpp"
#include <memory>
using namespace std;

namespace Clegg5300
{
void ForHead::initialize()
{
	auto type = lval->getType();

	// Start by allocating space
	oFile << "# Set aside space for the integer variable" << endl;
	oFile << "\taddi $sp, $sp, -" << type->getSize() << endl;
	unique_ptr<Register> reg;
	// Set the values to what they need to be for all expressions
	exp->emit();
	
	// If it is constant I need to get a register to store the value
	if (exp->isConst())
	{
		oFile << "# Moving the constant into a register" << endl;
		reg = Register::allocateRegister();
		oFile << "\tli " << *reg << ", " << exp->getValue() << endl;
	}
	else
	{	
		reg = exp->getRegister();
	}	
	auto address = lval->getAddress();
	address->emit();
	if (address->isConst())
	{
		auto addr = address->getValue();
		oFile << "\tsw " << *reg << ", ";
		if (lval->getMemLocation() == STACK)
			oFile << "-";
		oFile << addr << "(";
		if (lval->getMemLocation() != GLOBAL)
			oFile << "$gp)" << endl;
		else
			oFile << "$fp)" << endl;
	}
	else
	{
		auto addr = address->getRegister();
		oFile << "\tsw " << *reg << ", ";
		oFile << "0(" << *addr << ")" << endl;
	}
}

unique_ptr<Register> ForHead::loadValue()
{
	auto exp = std::make_shared<LoadExpression>(lval);
	exp->emit();
	return exp->getRegister();
}

void ForHead::storeValue(unique_ptr<Register> reg)
{	
	auto address = lval->getAddress();
	address->emit();
	if (address->isConst())
	{
		auto addr = address->getValue();
		oFile << "\tsw " << *reg << ", ";
		if (lval->getMemLocation() == STACK)
			oFile << "-";
		oFile << addr << "(";
		if (lval->getMemLocation() != GLOBAL)
			oFile << "$gp)" << endl;
		else
			oFile << "$fp)" << endl;
	}
	else
	{
		auto addr = address->getRegister();
		oFile << "\tsw " << *reg << ", ";
		oFile << "0(" << *addr << ")" << endl;
	}
}

void ForHead::restoreStack()
{
	auto type = lval->getType();
	oFile << "# Restore the stack to its old value" << endl;
	oFile << "\taddi $sp, $sp, " << type->getSize() << endl;
}

}
