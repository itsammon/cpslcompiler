#include "WhileStatement.hpp"
#include <iostream>
#include "LabelGenerator.hpp"
using namespace std;

namespace Clegg5300
{
void WhileStatement::emit()
{
	unique_ptr<Register> reg;
	// label the top and bottom of the while loop
	string topLabel = LabelGenerator::getLabel();
	string endLabel = LabelGenerator::getLabel();

	oFile << "# Begining of while loop" << endl;
	oFile << topLabel << ": ";

	// Set the values to what they need to be for all expressions
	exp->emit();
	
	// If it is constant I need to get a register to store the value
	if (exp->isConst())
	{
		oFile << "# Moving the constant into a register" << endl;
		reg = Register::allocateRegister();
		oFile << "\tli " << *reg << ", " << exp->getValue() << endl;
	}
	else
	{	
		reg = exp->getRegister();
	}

	// Output the condition check
	oFile << "\tbeq " << *reg << ", $zero, " << endLabel << endl;

	// Emit the statement sequence
	if (sequence != nullptr)
		sequence->emit();
	oFile << "\tj " << topLabel << endl;
	oFile << "#End of while loop" << endl;
	oFile << endLabel << ": " << endl;
}

}
