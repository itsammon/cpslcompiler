#ifndef ASSIGN_STATEMENT_HPP
#define ASSIGN_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include "Statement.hpp"
#include "LVal.hpp"
#include "Expression.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class AssignStatement : public Statement
{
	public:
		AssignStatement(std::shared_ptr<LVal> l, std::shared_ptr<Expression> e) : lval(l), exp(e) {}
		void emit();
		void storeVal(const std::unique_ptr<Register>&);
	private:
		std::shared_ptr<LVal> lval;
		std::shared_ptr<Expression> exp;
};

}

#endif
