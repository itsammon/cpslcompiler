#include "ToHead.hpp"
#include <iostream>
#include "Expression.hpp"
#include "AddExpression.hpp"
#include "LiteralExpression.hpp"
#include <memory>
using namespace std;

namespace Clegg5300
{
unique_ptr<Register> ToHead::expression()
{
	unique_ptr<Register> reg;
	auto lit = make_shared<LiteralExpression>(1);
	auto newExp = make_shared<AddExpression>(exp, lit);
	// Set the values to what they need to be for all expressions
	newExp->emit();
	
	// If it is constant I need to get a register to store the value
	if (newExp->isConst())
	{
		oFile << "# Moving the constant into a register" << endl;
		reg = Register::allocateRegister();
		oFile << "\tli " << *reg << ", " << newExp->getValue() << endl;
	}
	else
	{
		reg = newExp->getRegister();
	}
	return reg;
}
}
