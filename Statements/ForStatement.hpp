#ifndef FOR_STATEMENT_HPP
#define FOR_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include "ForHead.hpp"
#include "ToHead.hpp"
#include "Statement.hpp"
#include "StatementSequence.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ForStatement : public Statement
{
	public:
		ForStatement(std::shared_ptr<ForHead> h, shared_ptr<ToHead> t, std::shared_ptr<StatementSequence> seq) : head(h), to(t), sequence(seq) {}
		void emit();
	private:
		std::shared_ptr<ForHead> head;
		std::shared_ptr<ToHead> to;
		std::shared_ptr<StatementSequence> sequence;
};

}

#endif
