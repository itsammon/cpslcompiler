#include "ElseStatement.hpp"
#include <iostream>
using namespace std;

namespace Clegg5300
{
void ElseStatement::emit()
{
	oFile << "# Begining of else statement" << endl;
	// Emit the statement sequence
	if (sequence != nullptr)
		sequence->emit();
}

}
