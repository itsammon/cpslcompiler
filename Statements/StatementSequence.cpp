#include "StatementSequence.hpp"
#include <iostream>
using namespace std;

namespace Clegg5300
{
	void StatementSequence::add(shared_ptr<Statement> statement)
	{
		statements.push_back(statement);
	}

	void StatementSequence::emit()
	{
		for (auto && statement : statements)
		{
			if (statement != nullptr)
			{
				statement->emit();
			}
		}
	}
}
