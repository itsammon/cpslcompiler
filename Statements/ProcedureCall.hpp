#ifndef PROCEDURE_CALL_HPP
#define PROCEDURE_CALL_HPP

#include <iostream>
#include <fstream>
#include <memory>
#include "Function.hpp"
#include "Statement.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ProcedureCall : public Statement
{
	public:
		ProcedureCall(shared_ptr<Function> s, std::vector<shared_ptr<Expression>> e) : function(s), arguements(e) {};
		void emit();
	private:
		shared_ptr<Function> function;
		std::vector<std::shared_ptr<Expression>> arguements;		
};

}

#endif
