#ifndef WRITE_STATEMENT_HPP
#define WRITE_STATEMENT_HPP

#include <iostream>
#include <fstream>

#include "Statement.hpp"
#include "Expression.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class WriteStatement : public Statement
{
	public:
		WriteStatement(shared_ptr<Expression> e);
		void emit();
		void append(shared_ptr<Expression> l);
	private:
		std::vector<shared_ptr<Expression>> exps;
};



}

#endif
