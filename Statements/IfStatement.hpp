#ifndef IF_STATEMENT_HPP
#define IF_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include <memory>
#include "Statement.hpp"
#include "StatementSequence.hpp"
#include "ElseIfStatement.hpp"
#include "ElseStatement.hpp"
#include "Expression.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class IfStatement : public Statement
{
	public:
		IfStatement(std::shared_ptr<Expression> e, std::shared_ptr<StatementSequence> seq, std::shared_ptr<ElseIfStatement> eiState, std::shared_ptr<ElseStatement> eState) : exp(e), sequence(seq), eiStatement(eiState), eStatement(eState) {}
		void emit();
	private:
		std::shared_ptr<StatementSequence> sequence;
		std::shared_ptr<Expression> exp;
		std::shared_ptr<ElseIfStatement> eiStatement;
		std::shared_ptr<ElseStatement> eStatement;
};

}

#endif
