#ifndef RETURN_STATEMENT_HPP
#define RETURN_STATEMENT_HPP

#include <iostream>
#include <fstream>
#include <string>
#include "Statement.hpp"
#include "SymbolTable.hpp"
#include "LVal.hpp"
#include "Expression.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ReturnStatement : public Statement
{
	public:
		ReturnStatement(shared_ptr<Expression> e, shared_ptr<SymbolTable> t) : exp(e), table(t) { lval = make_shared<IdAccess>("_return", t); }
		void emit();

	private:
		std::shared_ptr<Expression> exp;
		std::shared_ptr<SymbolTable> table;
		std::shared_ptr<LVal> lval;
		
};

}

#endif
