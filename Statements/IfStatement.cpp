#include "IfStatement.hpp"
#include <iostream>
#include "Expression.hpp"
#include <memory>
#include "LabelGenerator.hpp"
using namespace std;

namespace Clegg5300
{
void IfStatement::emit()
{
	unique_ptr<Register> reg;
	string endLabel;
	string elseLabel;
	string elseIfLabel;

	// Set the values to what they need to be for all expressions
	exp->emit();
	
	// If it is constant I need to get a register to store the value
	if (exp->isConst())
	{
		oFile << "# Moving the constant into a register" << endl;
		reg = Register::allocateRegister();
		oFile << "\tli " << *reg << ", " << exp->getValue() << endl;
	}
	else
	{	
		reg = exp->getRegister();
	}
	
	// Generate a label for my else statement
	endLabel = LabelGenerator::getLabel();

	// Check if there is an else statement
	if (eStatement != nullptr)
	{
		elseLabel = LabelGenerator::getLabel();
	}
	else
	{
		elseLabel = endLabel;
	}

	// Check if there is an elseif statement
	if (eiStatement != nullptr)
	{
		elseIfLabel = eiStatement->getLabel();
	}
	else
	{
		elseIfLabel = elseLabel;
	}
	
	oFile << "#execute if statement" << endl;
	oFile << "\tbeq " << *reg << ", $zero, " << elseIfLabel << endl;
		
	// Emit our sequence
	if (sequence != nullptr)
	{
		oFile << "#If Statement Sequence" << endl;
		sequence->emit();
		oFile << "#End If Statement Sequence" << endl;
	}
	oFile << "\tj " << endLabel << endl;

	// If there is an ifelse part, we need to emit it
	if (eiStatement != nullptr)
	{
		eiStatement->emit(elseLabel, endLabel);
	}

	// If there is an else part, we need to emit it
	if (eStatement != nullptr)
	{
		// Output label for else statement
		oFile << elseLabel << ": ";
		eStatement->emit();
	}

	// Output the end of the if statement and the endlabel to jump to
	oFile << "# End of if statement" << endl;
	oFile << endLabel << ": " << endl;
	
}

}
