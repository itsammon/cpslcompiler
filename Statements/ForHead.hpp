#ifndef FOR_HEAD_HPP
#define FOR_HEAD_HPP

#include <iostream>
#include <fstream>
#include "LVal.hpp"
#include "Expression.hpp"
#include <memory>
#include "Register.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class ForHead
{
	public:
		ForHead(std::shared_ptr<LVal> l, std::shared_ptr<Expression> e) : lval(l), exp(e) {}
		void initialize();
		unique_ptr<Register> loadValue();
		void storeValue(unique_ptr<Register> reg);
		void restoreStack();
	private:
		std::shared_ptr<LVal> lval;
		std::shared_ptr<Expression> exp;
};

}

#endif
