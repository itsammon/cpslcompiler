#include "AssignStatement.hpp"
#include <iostream>
#include "Expression.hpp"
#include "LoadExpression.hpp"
#include "LVal.hpp"
#include <memory>
using namespace std;

namespace Clegg5300
{
void AssignStatement::emit()
{
	unique_ptr<Register> reg;

	if (exp->getType() == lval->getType())
	{
		exp->emit();
		cout << "Assigning expression of type " << exp->getType()->name() << endl;
		if (exp->getType()->name() == "Array" || exp->getType()->name() == "Record")
		{
			reg = Register::allocateRegister();
			oFile << "# Copying Structure" << endl;
			std::unique_ptr<Register> fromReg = exp->getRegister();			std::unique_ptr<Register> toReg;
			auto address = lval->getAddress();
			address->emit();
			if (!address->isConst())
				toReg = address->getRegister();
			
			int max = exp->getType()->getSize() / 4;
			
			for (int i = 0; i < max; i++)
			{
				oFile << "# Moving memory block" << endl;
				oFile << "\tlw " << *reg << ", " << (i*4);
				oFile << "(" << *fromReg << ")" << endl;
				
				if (address->isConst())
				{
					auto addr = address->getValue();
					oFile << "\tsw " << *reg << ", ";
					if (lval->getMemLocation() == STACK)
						oFile << "-";
					oFile << addr + (i*4) << "(";
					if (lval->getMemLocation() == GLOBAL)
						oFile << "$gp)" << endl;
					else
						oFile << "$fp)" << endl;
				}
				else
				{
					oFile << "\tsw " << *reg << ", ";
					oFile << (i*4);
					oFile << "(" << *toReg << ")" << endl;
				}	
			}

		}
		else
		{
			cout << "Assigning value to an lval" << endl;
			// If it is constant I need to get a register to store the value
			if (exp->isConst())
			{
				oFile << "# Moving the constant into a register" << endl;
				reg = Register::allocateRegister();
				oFile << "\tli " << *reg << ", " << exp->getValue() << endl;
			}
			else
			{	
				reg = exp->getRegister();
			}
			cout << lval->getType()->name() << endl;
			storeVal(reg);
		}
	}
}

void AssignStatement::storeVal(const std::unique_ptr<Register>& reg)
{
	cout << "Inside Store Value with a register" << endl;
	auto address = lval->getAddress();
	address->emit();
	oFile << "# Store the value in the appropriate address" << endl;		
	if (address->isConst())
	{
		auto addr = address->getValue();
		oFile << "\tsw " << *reg << ", ";
		if (lval->getMemLocation() == STACK)
			oFile << "-";
		oFile << addr << "(";
		if (lval->getMemLocation() != GLOBAL)
			oFile << "$gp)" << endl;
		else
			oFile << "$fp)" << endl;
	}
	else
	{
		auto addr = address->getRegister();
		oFile << "\tsw " << *reg << ", ";
		oFile << "0(" << *addr << ")" << endl;
	}
}

}
