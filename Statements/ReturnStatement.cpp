#include "ReturnStatement.hpp"
#include <iostream>
#include <memory>
#include "Register.hpp"
#include "SymbolTable.hpp"
#include "LVal.hpp"
#include "Expression.hpp"
#include "AssignStatement.hpp"
using namespace std;

namespace Clegg5300
{

void ReturnStatement::emit()
{
	auto assign = make_shared<AssignStatement>(lval, exp);
	unique_ptr<Register> reg;
	if (exp != nullptr && lval->getType() != nullptr)
	{
		if (exp->getType() != lval->getType())
		{
			cout << "Invalid return type. Found " << exp->getType()->name() << " expected " << lval->getType()->name() << endl;
			return;
		}
		assign->emit();
	}
	if (exp == nullptr && lval->getType() != nullptr)
	{
		//cout << "Invalid return type! Expected " << lval->getType()->name() << endl;
		return;
	}
	if (exp != nullptr && lval->getType() == nullptr)
	{
		cout << "Invalid return type! Found " << exp->getType()->name() << " expected no return type!" << endl;
		return;
	}
	
	oFile << "# Return statement" << endl;
	oFile << "\t j " << table->getLabel() << "_epi" << endl;
}

}
