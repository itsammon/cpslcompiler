#ifndef STATEMENT_SEQUENCE_HPP
#define STATEMENT_SEQUENCE_HPP

#include <vector>
#include <memory>
#include "Statement.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

class StatementSequence
{
	public:
		void add(std::shared_ptr<Statement>);
		void emit();
	private:
		std::vector<std::shared_ptr<Statement>> statements;
};



}

#endif
