#ifndef FUNCTION_CALL_HPP
#define FUNCTION_CALL_HPP

#include <fstream>
#include <memory>
#include "Function.hpp"
#include "Expression.hpp"
#include "LVal.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

/* Create a class to store expressions */
class FunctionCall : public Expression
{
	public:
		FunctionCall(shared_ptr<Function> s, std::vector<shared_ptr<Expression>> e) : function(s), arguements(e) 
		{ rVal = make_shared<IdAccess>("_return", function->getTable()); }
		void emit();
		bool isConst() { return false; }
		std::shared_ptr<Type> getType() { return rVal->getType(); }
	protected:
		shared_ptr<Function> function;
		std::vector<std::shared_ptr<Expression>> arguements;	
		shared_ptr<LVal> rVal;
};

}
#endif
