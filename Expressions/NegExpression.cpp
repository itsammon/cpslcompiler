#include "NegExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

NegExpression::NegExpression(shared_ptr<Expression> e)
{
	exp = e;
}

void NegExpression::emit()
{
	exp->emit();
	setValue();
}

void NegExpression::setValue()
{
	if (!exp->isConst())
	{
		reg = Register::allocateRegister();
		oFile << "\tsub " << *reg << ", $0, " << *(exp->getRegister()) << endl;
	}
	else
	{
		value = -exp->getValue();
	}
}

bool NegExpression::isConst()
{
	return (exp->isConst());

}

shared_ptr<Type> NegExpression::getType()
{
	if (!exp->getType()) return nullptr;
	return exp->getType();
}

} // End namespace
