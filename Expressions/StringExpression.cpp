#include "StringExpression.hpp"
#include "Type.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

StringExpression::StringExpression(int i)
{
	value = i;
}

void StringExpression::emit()
{
	
}

bool StringExpression::isConst()
{
	return true;
}

shared_ptr<Type> StringExpression::getType()
{
	return BuiltInType::getString();
}

} // End namespace
