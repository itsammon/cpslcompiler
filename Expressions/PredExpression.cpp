#include "PredExpression.hpp"
#include "Type.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

PredExpression::PredExpression(shared_ptr<Expression> e)
{
	exp = e;
}

void PredExpression::emit()
{
	exp->emit();
	setValue();
}

void PredExpression::setValue()
{
	if (!exp->isConst())
	{
		reg = Register::allocateRegister();
		if (exp->getType() == BuiltInType::getBool())
		{
			oFile << "# We want the predecessor of bool to be its opposite" << endl;
			oFile << "\tseq " << *reg << ", $0, " << *(exp->getRegister()) << endl;
		}
		else
		{
			oFile << "\taddi " << *reg << ", " << *(exp->getRegister()) << ", -1 " << endl;
		}
	}
	else
	{
		if (exp->getType() == BuiltInType::getBool())
		{
			if (exp->getValue() == 0)
				value = 1;
			else
				value = 0;
		}
		else
		{
			value = exp->getValue()-1;
		}
	}
}

bool PredExpression::isConst()
{
	return (exp->isConst());
}

shared_ptr<Type> PredExpression::getType()
{
	if (!exp->getType()) return nullptr;
	return exp->getType();
}

} // End namespace
