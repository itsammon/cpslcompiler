#include "NotEqualExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
#include "LabelGenerator.hpp"
using namespace std;

namespace Clegg5300
{

NotEqualExpression::NotEqualExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void NotEqualExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void NotEqualExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		auto label1 = LabelGenerator::getLabel();
		auto label2 = LabelGenerator::getLabel();
		if (!second->isConst())
		{
			oFile << "\tbeq " << *(first->getRegister()) << ", " << *(second->getRegister()) << ", " << label1 << endl;
			oFile << "\taddi " << *reg << ", $zero , 1" << endl;
			oFile << "\tj " << label2 << endl;
			oFile << label1 << ":add " << *reg << ", $zero, $zero" << endl;
			oFile << label2 << ": ";
		}
		else
		{
			oFile << "#load in the value of the second exp " << endl;
			oFile << "\tli " << *reg << ", " << second->getValue() << endl;
			oFile << "\tbeq " << *(first->getRegister()) << ", " << *reg << ", " << label1 << endl;
			oFile << "\taddi " << *reg << ", $zero , 1" << endl;
			oFile << "\tj " << label2 << endl;
			oFile << label1 << ":add " << *reg << ", $zero, $zero" << endl;
			oFile << label2 << ": ";
		}	
	}
	else
	{
		if (!second->isConst())
		{
			auto label1 = LabelGenerator::getLabel();
			auto label2 = LabelGenerator::getLabel();
			oFile << "#load in the value of the first exp " << endl;
			oFile << "\tli " << *reg << ", " << first->getValue() << endl;
			oFile << "\tbeq " << *(second->getRegister()) << ", " << *reg << ", " << label1 << endl;
			oFile << "\taddi " << *reg << ", $zero , 1" << endl;
			oFile << "\tj " << label2 << endl;
			oFile << label1 << ":add " << *reg << ", $zero, $zero" << endl;
			oFile << label2 << ": ";
		}
		else
		{
			value = (first->getValue() == second->getValue());
		}
	}
}

bool NotEqualExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> NotEqualExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
