#ifndef STRING_EXPRESSION_HPP
#define STRING_EXPRESSION_HPP

#include "Expression.hpp"

#include <memory>
#include <fstream>

extern std::ofstream oFile;

namespace Clegg5300
{

class StringExpression : public Expression
{
	public:
		StringExpression(int i);
		void emit();
		bool isConst();
		std::shared_ptr<Type> getType();
	private:
};


}

#endif
