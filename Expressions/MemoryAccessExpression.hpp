#ifndef MEMORY_ACCESS_EXPRESSION_HPP
#define MEMORY_ACCESS_EXPRESSION_HPP

#include <fstream>
#include <memory>
#include "Type.hpp"
#include "Register.hpp"
#include "Expression.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

/* Create a class to store expressions */
class MemoryAccessExpression : public Expression
{
	public:
		MemoryAccessExpression(int memValue, int off);
		MemoryAccessExpression(std::shared_ptr<Expression> memLoc);
		bool isConst() { return false; }
		void emit();
		std::shared_ptr<Type> getType();
	private:
		std::shared_ptr<Type> type;
		std::shared_ptr<Expression> offset;
};

}
#endif
