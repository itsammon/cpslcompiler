#include "LiteralExpression.hpp"
#include "Type.hpp"

#include <memory>
using namespace std;

namespace Clegg5300
{

LiteralExpression::LiteralExpression(int v) : Expression()
{
	value = v;
	type = BuiltInType::getInt();
}

LiteralExpression::LiteralExpression(char c) : Expression()
{
	value = static_cast<int>(c);
	type = BuiltInType::getChar();
}

LiteralExpression::LiteralExpression(bool b) : Expression()
{
	value = b;
	type = BuiltInType::getBool();
}

void LiteralExpression::emit()
{
	// Do nothing since all values are manipulated internally.
}

shared_ptr<Type> LiteralExpression::getType()
{
	return type;
}


bool LiteralExpression::isConst()
{
	return true;

}

}
