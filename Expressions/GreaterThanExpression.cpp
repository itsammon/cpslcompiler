#include "GreaterThanExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

GreaterThanExpression::GreaterThanExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void GreaterThanExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void GreaterThanExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tslt " << *reg << "," << *(second->getRegister()) << "," << *(first->getRegister()) << endl;	
		}
		else
		{
			oFile << "\tli " << *reg << ", " << second->getValue() << endl;
			oFile << "\tslt " << *reg << ", " << *reg << ", " << *(first->getRegister()) << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "\tslti " << *reg << "," << *(second->getRegister()) << ", " << first->getValue() << endl;
		}
		else
		{
			value = (first->getValue() > second->getValue());
		}
	}
}

bool GreaterThanExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> GreaterThanExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
