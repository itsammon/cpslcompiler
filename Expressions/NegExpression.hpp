#ifndef NEG_EXPRESSION_HPP
#define NEG_EXPRESSION_HPP

#include "Expression.hpp"

#include <memory>
#include <fstream>

extern std::ofstream oFile;

namespace Clegg5300
{

class NegExpression : public Expression
{
	public:
		NegExpression(std::shared_ptr<Expression>);
		void emit();
		bool isConst();
		std::shared_ptr<Type> getType();
	private:
		void setValue();
		std::shared_ptr<Expression> exp;
};


}

#endif
