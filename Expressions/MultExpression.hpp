#ifndef MULT_EXPRESSION_HPP
#define MULT_EXPRESSION_HPP

#include "Expression.hpp"

#include <memory>
#include <fstream>

extern std::ofstream oFile;

namespace Clegg5300
{

class MultExpression : public Expression
{
	public:
		MultExpression(std::shared_ptr<Expression>, std::shared_ptr<Expression>);
		void emit();
		bool isConst();
		std::shared_ptr<Type> getType();
	private:
		void setValue();
		std::shared_ptr<Expression> first;
		std::shared_ptr<Expression> second;
};


}

#endif
