#include "FunctionCall.hpp"
#include "Function.hpp"
#include "Register.hpp"
#include <iostream>
using namespace std;

namespace Clegg5300
{

void FunctionCall::emit()
{
	auto argSpace = 0;
	auto regSpace = 0;
	oFile << "# Function Call to " << function->getSignature()->getLabel() << endl;
	// Spill Registers
	auto usedRegisters = Register::getInUse();
	regSpace = usedRegisters.size() * 4;
	// Get the number of register to spill
	regSpace += 8; // Add extra space for fp and ra
	auto storeLoc = 0;
	oFile << "\taddi $sp, $sp, -" << regSpace << endl;

	oFile << "# Spill Registers" << endl;
	for (auto regist : usedRegisters)
	{
		oFile << "\tsw " << regist << ", " << storeLoc << "($fp)" << endl;
		storeLoc += 4;
	}
	oFile << "\tsw $ra, " << storeLoc << "($sp)" << endl;
	storeLoc += 4;
	oFile << "\tsw $fp, " << storeLoc << "($sp)" << endl;
	
	// Move Arguements
	for (auto &&arg : arguements)
	{
		arg->emit();
		argSpace += arg->getType()->getSize();	
	}
	if (argSpace != 0)
	{
		oFile << "# Allocate space on stack for arguements" << endl;
		oFile << "\taddi $sp, $sp, -" << argSpace << endl;
	}
	// Reset the space while we add each arguement in order
	auto offset = 0;
	for (auto &&arg : arguements)
	{
		offset -= arg->getType()->getSize();
		if (std::dynamic_pointer_cast<ArrayType>(arg->getType()) || std::dynamic_pointer_cast<RecordType>(arg->getType()))
		{
			reg = Register::allocateRegister();
			oFile << "# Copying Structure" << endl;
			std::unique_ptr<Register> fromReg = arg->getRegister();
			int max = arg->getType()->getSize() / 4;
			
			for (int i = 0; i < max; i++)
			{
				oFile << "# Moving memory block" << endl;
				oFile << "\tlw " << *reg << ", " << (i*4);
				oFile << "(" << *fromReg << ")" << endl;
				
				oFile << "\tsw " << *reg << ", ";
				oFile << "-";
				oFile << offset + (i*4) << "(";
				oFile << "$fp)" << endl;
			}

		}
		else if (arg->isConst())
		{
			std::unique_ptr<Register> reg = Register::allocateRegister();
			oFile << "\tli " << *(reg) << ", " << arg->getValue() << endl;
			oFile << "\tsw " << *(reg) << ", " << offset << "($sp)" << endl;
		}
		else
		{
			std::unique_ptr<Register> reg = arg->getRegister();
			oFile << "\tsw " << *(reg) << ", " << offset << "($sp)" << endl;
		}
	}

	// Set fp to new location
	oFile << "\tmove $fp, $sp" << endl;
	oFile << "\tjal " << function->getSignature()->getLabel() << endl;

	// Handle return type
	// Setup this way to allow the register to be restored
	{
		// Get value into register
		auto addr = rVal->getAddress();
		addr->emit();
		std::unique_ptr<Register> reg = addr->getRegister();
		oFile << "\tmove $v1, " << *(reg) << endl;
	}
	
	// Set sp back to below args
	oFile << "# Move sp below arguements" << endl;
	oFile << "\taddi $sp, $sp, " << argSpace << endl;
	// Load Spilled Registers
	storeLoc = 0;	
	oFile << "# Restore Registers" << endl;
	for (auto regist : usedRegisters)
	{
		oFile << "\tlw " << regist << ", " << storeLoc << "($fp)" << endl;
		storeLoc += 4;
	}

	oFile << "\tlw $ra, " << storeLoc << "($sp)" << endl;
	// Load fp
	storeLoc += 4;
	oFile << "\tlw $fp, " << storeLoc << "($sp)" << endl;
	oFile << "# Restore sp" << endl;
	storeLoc += 4;
	oFile << "\taddi $sp, $sp, " << storeLoc << endl;
	
	reg = Register::allocateRegister();
	oFile << "# Get the value into a normal register" << endl;
	oFile << "\tmove " << *(reg) << ", $v1" << endl;
}

}
