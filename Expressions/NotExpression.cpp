#include "NotExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

NotExpression::NotExpression(shared_ptr<Expression> e)
{
	exp = e;
}

void NotExpression::emit()
{
	exp->emit();
	setValue();
}

void NotExpression::setValue()
{
	if (!exp->isConst())
	{
		reg = Register::allocateRegister();
		oFile << "\tnot " << *reg << "," << *(exp->getRegister()) << endl;
	}
	else
	{
		value = ~exp->getValue();
	}
}

bool NotExpression::isConst()
{
	return (exp->isConst());

}

shared_ptr<Type> NotExpression::getType()
{
	if (!exp->getType()) return nullptr;
	return exp->getType();
}

} // End namespace
