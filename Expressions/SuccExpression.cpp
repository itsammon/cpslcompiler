#include "SuccExpression.hpp"
#include "Type.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

SuccExpression::SuccExpression(shared_ptr<Expression> e)
{
	exp = e;
}

void SuccExpression::emit()
{
	exp->emit();
	setValue();
}

void SuccExpression::setValue()
{
	if (!exp->isConst())
	{
		reg = Register::allocateRegister();
		if (exp->getType() == BuiltInType::getBool())
		{
			oFile << "# We want the predecessor of bool to be its opposite" << endl;
			oFile << "\tseq " << *reg << ", $0, " << *(exp->getRegister()) << endl;
		}
		else
		{
			oFile << "\taddi " << *reg << ", " << *(exp->getRegister()) << ", 1 " << endl;
		}
	}
	else
	{
		if (exp->getType() == BuiltInType::getBool())
		{
			if (exp->getValue() == 0)
				value = 1;
			else
				value = 0;
		}
		else
		{
			value = exp->getValue()+1;
		}
	}
}

bool SuccExpression::isConst()
{
	return (exp->isConst());
}

shared_ptr<Type> SuccExpression::getType()
{
	if (!exp->getType()) return nullptr;
	return exp->getType();
}

} // End namespace
