#include "SubExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

SubExpression::SubExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void SubExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void SubExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tsub " << *reg << "," << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
		}
		else
		{
			oFile << "\tsubi " << *reg << ", " << *(first->getRegister()) << ", " << second->getValue() << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			// Need to load in the first value
			oFile << "\tli " << *reg << ", " << first->getValue();
			oFile << "\tsubi " << *reg << "," << *reg << ", " << *(second->getRegister()) << endl;
		}
		else
		{
			value = first->getValue() - second->getValue();
		}
	}
}

bool SubExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> SubExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
