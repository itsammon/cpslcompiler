#include "AndExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

AndExpression::AndExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void AndExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void AndExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tand " << *reg << "," << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
		}
		else
		{
			oFile << "\tandi " << *reg << ", " << *(first->getRegister()) << ", " << second->getValue() << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "\tandi " << *reg << "," << *(second->getRegister()) << ", " << first->getValue() << endl;
		}
		else
		{
			value = first->getValue() & second->getValue();
		}
	}
}

bool AndExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> AndExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
