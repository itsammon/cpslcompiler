#include "AddExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

AddExpression::AddExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void AddExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void AddExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tadd " << *reg << "," << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
		}
		else
		{
			oFile << "\taddi " << *reg << ", " << *(first->getRegister()) << ", " << second->getValue() << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "\taddi " << *reg << "," << *(second->getRegister()) << ", " << first->getValue() << endl;
		}
		else
		{
			value = first->getValue() + second->getValue();
		}
	}
}

bool AddExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> AddExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
