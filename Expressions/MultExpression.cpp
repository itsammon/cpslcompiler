#include "MultExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

MultExpression::MultExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void MultExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void MultExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tmult " << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
			oFile << "\tmflo " << *reg << endl;
		}
		else
		{
			// Move value into a register so we can multiply it
			oFile << "\tli " << *reg << ", " << second->getValue() << endl;
			oFile << "\tmult " << *reg << ", " << *(first->getRegister())  << endl;
			oFile << "\tmflo " << *reg << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			
			// Move value into a register so we can multiply it
			oFile << "\tli " << *reg << ", " << first->getValue() << endl;
			oFile << "\tmult " << *reg << ", " << *(second->getRegister())  << endl;
			oFile << "\tmflo " << *reg << endl;
		}
		else
		{
			value = first->getValue() * second->getValue();
		}
	}
}

bool MultExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> MultExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
