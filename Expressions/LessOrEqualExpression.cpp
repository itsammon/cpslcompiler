#include "LessOrEqualExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

LessOrEqualExpression::LessOrEqualExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void LessOrEqualExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void LessOrEqualExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "#LessThanOrEqual expression" << endl;
			oFile << "\tslt " << *reg << "," << *(second->getRegister()) << "," << *(first->getRegister()) << endl;		
			oFile << "\tori " << *reg << ", " << *reg << ", 1" << endl;
		}
		else
		{
			oFile << "#LessThanOrEqual expression" << endl;
			oFile << "\tli " << *reg << ", " << second->getValue() << endl;
			oFile << "\tslt " << *reg << ", " << *reg << ", " << *(first->getRegister()) << endl;	
			oFile << "\tori " << *reg << ", " << *reg << ", 1" << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "#LessThanOrEqual expression" << endl;
			oFile << "\tslti " << *reg << "," << *(second->getRegister()) << ", " << first->getValue() << endl;	
			oFile << "\tori " << *reg << ", " << *reg << ", 1" << endl;
		}
		else
		{
			value = (first->getValue() > second->getValue());
		}
	}
}

bool LessOrEqualExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> LessOrEqualExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
