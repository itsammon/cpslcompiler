#include "Expression.hpp"
namespace Clegg5300
{

std::unique_ptr<Register> Expression::getRegister()
{
	return std::move(reg);
}

int Expression::getValue()
{
	return value;
}

}
