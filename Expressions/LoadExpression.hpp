#ifndef LOAD_EXPRESSION_HPP
#define LOAD_EXPRESSION_HPP

#include <memory>

#include "Expression.hpp"
#include "LVal.hpp"
#include "Register.hpp"

namespace Clegg5300
{

class LoadExpression : public Expression
{
	public:
		LoadExpression(std::shared_ptr<LVal> lVal); 
		void emit();
		bool isConst();
		std::shared_ptr<Type> getType();
		string getLocation(int);
	private:
		shared_ptr<LVal> lval;
		bool reference;
};		

}

#endif
