#ifndef LITERAL_EXPRESSION_HPP
#define LITERAL_EXPRESSION_HPP

#include "Expression.hpp"

#include <memory>

namespace Clegg5300
{

class LiteralExpression : public Expression
{
	public:
		LiteralExpression(int v);
		LiteralExpression(char c);
		LiteralExpression(bool b);
		bool isConst();
		void emit();
		shared_ptr<Type> getType();
	private:
		std::shared_ptr<Type> type;
		
};


}

#endif
