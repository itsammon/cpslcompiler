#include "DivideExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

DivideExpression::DivideExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void DivideExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void DivideExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tdiv " << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
			oFile << "\tmflo " << *reg << endl;
		}
		else
		{
			// Move value into a register so we can multiply it
			oFile << "\tli " << *reg << ", " << second->getValue() << endl;
			oFile << "\tdiv " << *(first->getRegister()) << ", "  << *reg << endl;
			oFile << "\tmflo " << *reg << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			
			// Move value into a register so we can multiply it
			oFile << "\tli " << *reg << ", " << first->getValue() << endl;
			oFile << "\tdiv " << *reg << ", " << *(second->getRegister())  << endl;
			oFile << "\tmflo " << *reg << endl;
		}
		else
		{
			value = first->getValue() / second->getValue();
		}
	}
}

bool DivideExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> DivideExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
