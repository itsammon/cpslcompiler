#include "MemoryAccessExpression.hpp"
#include "Expression.hpp"
#include "LiteralExpression.hpp"
#include "Symbol.hpp"
#include <memory>
using namespace std;
namespace Clegg5300
{

MemoryAccessExpression::MemoryAccessExpression(int memValue, int off)
	: type(nullptr), offset(std::make_shared<LiteralExpression>(off))
{
	value = memValue;
}

MemoryAccessExpression::MemoryAccessExpression(std::shared_ptr<Expression> memLoc) : type(nullptr), offset(memLoc)
{
	value = 0;
}

void MemoryAccessExpression::emit()
{
	reg = Register::allocateRegister();
	if (offset->isConst())
	{
		if (value == GLOBAL)
			oFile << "\taddi " << *reg << ", $gp, " << offset->getValue() << endl;
		else if (value == STACK)
			oFile << "\taddi " << *reg << ", $fp, " << - offset->getValue() << endl;
		else if (value == FRAME)
			oFile << "\taddi " << *reg << ", $fp, " << - offset->getValue() << endl;
	}
	else
	{
		offset->emit();
		std::unique_ptr<Register> toReg = offset->getRegister();
		oFile << "\tlw " << *reg << ", 0(" << *toReg << ")" << endl;
	}
}

std::shared_ptr<Type> MemoryAccessExpression::getType()
{
	return type;
}

}
