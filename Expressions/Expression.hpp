#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <fstream>
#include <memory>
#include "Type.hpp"
#include "Register.hpp"

// Forward declaration of output file
extern std::ofstream oFile;

namespace Clegg5300
{

/* Create a class to store expressions */
class Expression
{
	public:
		Expression() {};
		virtual bool isConst() = 0;
		virtual void emit() = 0;
		virtual std::shared_ptr<Type> getType() = 0;
		std::unique_ptr<Register> getRegister();
		int getValue();
	protected:
		int value;
		std::unique_ptr<Register> reg;
};

}
#endif
