#include "LoadExpression.hpp"
#include <iostream>
#include <memory>

namespace Clegg5300
{
LoadExpression::LoadExpression(std::shared_ptr<LVal> lVal)
	: Expression(), reference(false)
{
	lval = lVal;
}

void LoadExpression::emit()
{
	// Add in check for reference variables
	if (!reference && isConst())
	{
		// Run a load value variable
		value = lval->getValue();
	}
	else
	{
		oFile << "# Load an lval" << endl;
		auto address = lval->getAddress();
		address->emit();
		reg = Register::allocateRegister();
		std::unique_ptr<Register> toReg = address->getRegister();
		
		if (reference)
		{
			oFile << "\taddi " << *reg << ", " << *toReg << ", 0" << endl;
		}
		else if (!(std::dynamic_pointer_cast<ArrayType>(lval->getType()) || std::dynamic_pointer_cast<RecordType>(lval->getType())))
		{
			oFile << "\tlw " << *reg << ", 0(" << *toReg << ")" << endl;		
		}
		else
		{
			oFile << "\taddi " << *reg << ", " << *toReg << ", 0" << endl;
		}
	}
}

shared_ptr<Type> LoadExpression::getType()
{
	return lval->getType();
}

bool LoadExpression::isConst()
{
	return lval->isConst();
}

}
