#include "LessThanExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

LessThanExpression::LessThanExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void LessThanExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void LessThanExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tslt " << *reg << "," << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
		}
		else
		{
			oFile << "\tslti " << *reg << ", " << *(first->getRegister()) << ", " << second->getValue() << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "\tli " << *reg << ", " << first->getValue() << endl;
			oFile << "\tslt " << *reg << "," << *reg << ", " << *(second->getRegister()) << endl;
		}
		else
		{
			value = (first->getValue() < second->getValue());
		}
	}
}

bool LessThanExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> LessThanExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
