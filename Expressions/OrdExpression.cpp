#include "OrdExpression.hpp"
#include "Type.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

OrdExpression::OrdExpression(shared_ptr<Expression> e)
{
	exp = e;
}

void OrdExpression::emit()
{
	exp->emit();
	setValue();
}

void OrdExpression::setValue()
{
	if (!exp->isConst())
	{
		reg = exp->getRegister();
	}
	else
	{
		value = exp->getValue();
	}
}

bool OrdExpression::isConst()
{
	return (exp->isConst());
}

shared_ptr<Type> OrdExpression::getType()
{
	return BuiltInType::getInt();
}

} // End namespace
