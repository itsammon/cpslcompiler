#include "ChrExpression.hpp"
#include "Type.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

ChrExpression::ChrExpression(shared_ptr<Expression> e)
{
	exp = e;
}

void ChrExpression::emit()
{
	exp->emit();
	setValue();
}

void ChrExpression::setValue()
{
	if (!exp->isConst())
	{
		reg = exp->getRegister();
	}
	else
	{
		value = exp->getValue();
	}
}

bool ChrExpression::isConst()
{
	return (exp->isConst());
}

shared_ptr<Type> ChrExpression::getType()
{
	return BuiltInType::getChar();
}

} // End namespace
