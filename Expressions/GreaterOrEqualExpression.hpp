#ifndef GREATER_OR_EQUAL_EXPRESSION_HPP
#define GREATER_OR_EQUAL_EXPRESSION_HPP

#include "Expression.hpp"

#include <memory>
#include <fstream>

extern std::ofstream oFile;

namespace Clegg5300
{

class GreaterOrEqualExpression : public Expression
{
	public:
		GreaterOrEqualExpression(std::shared_ptr<Expression>, std::shared_ptr<Expression>);
		void emit();
		bool isConst();
		std::shared_ptr<Type> getType();
	private:
		void setValue();
		std::shared_ptr<Expression> first;
		std::shared_ptr<Expression> second;
};


}

#endif
