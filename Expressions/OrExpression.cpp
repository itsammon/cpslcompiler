#include "OrExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

OrExpression::OrExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void OrExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void OrExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "\tor " << *reg << "," << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
		}
		else
		{
			oFile << "\tori " << *reg << ", " << *(first->getRegister()) << ", " << second->getValue() << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "\tori " << *reg << "," << *(second->getRegister()) << ", " << first->getValue() << endl;
		}
		else
		{
			value = first->getValue() | second->getValue();
		}
	}
}

bool OrExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> OrExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
