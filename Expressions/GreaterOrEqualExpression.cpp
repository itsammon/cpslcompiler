#include "GreaterOrEqualExpression.hpp"

#include <iostream>
#include <fstream>
#include <memory>
using namespace std;

namespace Clegg5300
{

GreaterOrEqualExpression::GreaterOrEqualExpression(shared_ptr<Expression> firstArg, shared_ptr<Expression> secondArg)
{
	first = firstArg;
	second = secondArg;
}

void GreaterOrEqualExpression::emit()
{
	first->emit();
	second->emit();
	setValue();
}

void GreaterOrEqualExpression::setValue()
{
	if (!first->isConst())
	{
		reg = Register::allocateRegister();
		if (!second->isConst())
		{
			oFile << "#GreaterThanOrEqual expression" << endl;
			oFile << "\tslt " << *reg << "," << *(first->getRegister()) << "," << *(second->getRegister()) << endl;	
			oFile << "\tori " << *reg << ", " << *reg << ", 1" << endl;
		}
		else
		{
			oFile << "#GreaterThanOrEqual expression" << endl;
			oFile << "\tslti " << *reg << ", " << *(first->getRegister()) << ", " << second->getValue() << endl;
			oFile << "\tori " << *reg << ", " << *reg << ", 1" << endl;
		}	
	}
	else
	{
		if (!second->isConst())
		{
			reg = Register::allocateRegister();
			oFile << "#GreaterThanOrEqual expression" << endl;
			oFile << "\tli " << *reg << ", " << first->getValue() << endl;
			oFile << "\tslt " << *reg << "," << *reg << ", " << *(second->getRegister()) << endl;
			oFile << "\tori " << *reg << ", " << *reg << ", 1" << endl;
		}
		else
		{
			value = (first->getValue() < second->getValue());
		}
	}
}

bool GreaterOrEqualExpression::isConst()
{
	return (first->isConst() && second->isConst());

}

shared_ptr<Type> GreaterOrEqualExpression::getType()
{
	if (!first->getType()) return nullptr;
	if (!second->getType()) return nullptr;
	if (first->getType() != second->getType()) return nullptr;
	return first->getType();
}

} // End namespace
