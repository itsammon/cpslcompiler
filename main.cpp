#include <iostream>
#include <fstream>
#include <string>
#include "StringTable.hpp"
#include "Front.hpp"
using namespace std;
using namespace Clegg5300;

#include <boost/program_options.hpp>

// Forward declarations of oFile
ofstream oFile;
StringTable stringTable;

namespace po = boost::program_options;

/*
Runs the program and parses command line arguemnts
*/
int main(int argc, char* argv[])
{
	try
	{
		std::string iFile = "";
		std::string oFile = "";

		po::options_description desc("Allowed options");
		desc.add_options()("help,h", "Produce help message")(
		 "input,i", po::value<std::string>(), "Input cpsl file")("output,o", po::value<std::string>(), "Output asm file")("comments,c", "Output comments");
		
		po::positional_options_description positionalOptions;
		positionalOptions.add("input", 1);
		positionalOptions.add("ouput", 1);

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			cout << desc << endl;
			return 1;
		}
	
		if (vm.count("comments"))
		{
			//TODO Add output control
		}
		if (vm.count("output"))
		{
			oFile = vm["output"].as<std::string>();		
		}
		else
		{
			oFile = "out.asm";
		}
		if (vm.count("input"))
		{
			iFile = vm["input"].as<std::string>();
		}
		else
		{
			iFile = "in.cpsl";
		}

		parseCPSL(iFile, oFile);
	}
	catch(std::exception &e)
	{
		cout << "ERROR: " << e.what();
		return EXIT_FAILURE;
	}
}

