#ifndef LABEL_GENERATOR_HPP
#define LABEL_GENERATOR_HPP

#include <string>
#include <iostream>

class LabelGenerator
{
	public:
		static std::string getLabel()
		{
			std::string label = std::string("L");
			label += std::to_string(numLabel);
			numLabel++;
			return label;
		};
	private:
		static int numLabel;
};


#endif
