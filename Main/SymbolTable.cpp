#include "SymbolTable.hpp"

#include <iostream>
#include <map>

#include "Type.hpp"
using namespace std;
namespace Clegg5300
{
/*
Looks up a type in the symbol table
*/
shared_ptr<Type> SymbolTable::lookupType(string id)
{
	// Find the id in the map
	auto found = types.find(id);
	if (found != types.end())
	{
		// Return the value not the id
		return found->second;
	}
	// Check in the next table up
	if (parent) return parent->lookupType(id);
	// If not found return nullptr
	return nullptr;
}

/*
Looks up a variable in the symbol table
*/
shared_ptr<Symbol> SymbolTable::lookupVariable(string id)
{
	// Find the id in the map
	auto found = variables.find(id);
	if (found != variables.end())
	{
		// Return the value not the id
		return found->second;
	}
	// Check in the next table up
	if (parent) return parent->lookupVariable(id);
	// If not found return nullptr
	return nullptr;
}

/*
Looks up a variable in the symbol table
*/
shared_ptr<Expression> SymbolTable::lookupConst(string id)
{
	// Find the id in the map
	auto found = consts.find(id);
	if (found != consts.end())
	{
		// Return the value not the id
		return found->second;
	}
	// Check in the next table up
	if (parent) return parent->lookupConst(id);
	// If not found return nullptr
	return nullptr;
}

void SymbolTable::addType(string id, shared_ptr<Type> type)
{
	// Check if it is in the map already
	auto found = types.find(id);
	// If it is not add it
	if (found == types.end())
	{
		types[id] = type;
	}
}
void SymbolTable::addVariable(string id, shared_ptr<Type> type)
{
	auto found = variables.find(id);
	if (found == variables.end())
	{
		// If it is a local variable change the memory offset first
		if (memLocation == STACK && type != nullptr)
		{
			memoryOffset += type->getSize();
		}
		variables[id] = 
			std::make_shared<Symbol>(id, type, memLocation, memoryOffset);
		// Set the offset for the memory afterwards if it is global
		if (memLocation == GLOBAL && type != nullptr)
		{
			memoryOffset += type->getSize();
		}
	}
}


void SymbolTable::addParameter(string id, shared_ptr<Type> type)
{
	auto found = variables.find(id);
	if (found == variables.end())
	{
		variables[id] = 
			std::make_shared<Symbol>(id, type, FRAME, parameterOffset);
		parameterOffset += type->getSize();
	}
}


void SymbolTable::addConst(string id, shared_ptr<Expression> constant)
{
	// Check if it is in the map already
	auto found = consts.find(id);
	// If it is not add it
	if (found == consts.end())
	{
		consts[id] = constant;
	}
}

}
