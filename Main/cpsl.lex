%top{
#include <iostream>
#include <string>
using namespace std;

int lineNum = 1;
#define YY_DECL extern "C" int yylex()

#include "parser.hpp"

}
%option noyywrap

DIGIT	[0-9]
OCTAL	[0-7]
HEX	[A-Fa-f0-9]
ALPHA	[a-zA-Z]
ID	[a-zA-Z][a-zA-Z0-9_]*

%%
"\n"	++lineNum;
"+"	{ return '+'; }
"-"	{ return '-'; }
"*"	{ return '*'; }
"/"	{ return '/'; }
"&"	{ return '&'; }
"|"	{ return '|'; }
"~"	{ return '~'; }
"="	{ return '='; }
"<>"	{ return EQUAL; }
"<="	{ return LESSOREQUAL; }
"<"	{ return '<'; }
">="	{ return GREATEROREQUAL; }
">"	{ return '>'; }
"."	{ return '.'; }
","	{ return ','; }
":="	{ return ASSIGN; }
":"	{ return ':'; }
";"	{ return ';'; }
"("	{ return '('; }
")"	{ return ')'; }
"["	{ return '['; }
"]"	{ return ']'; }
"%"	{ return '%'; }

array|ARRAY	{ return ARRAY; }
begin|BEGIN	{ return START; }
chr|CHR		{ return CHR; }
const|CONST	{ return CONST; }
do|DO		{ return DO; }
downto|DOWNTO	{ return DOWNTO; }
else|ELSE	{ return ELSE; }
elseif|ELSEIF	{ return ELSEIF; }
end|END		{ return END; }
for|FOR		{ return FOR; }
forward|FORWARD	{ return FORWARD; }
function|FUNCTION	{ return FUNCTION; }
if|IF		{ return IF; }
of|OF		{ return OF; }
ord|ORD		{ return ORD; }
pred|PRED	{ return PRED; }
procedure|PROCEDURE	{ return PROCEDURE; }
read|READ	{ return READ; }
record|RECORD	{ return RECORD; }
ref|REF		{ return REF; }
repeat|REPEAT	{ return REPEAT; }
return|RETURN	{ return RETURN; }
stop|STOP	{ return STOP; }
succ|SUCC	{ return SUCC; }
then|THEN	{ return THEN; }
to|TO		{ return TO; }
type|TYPE	{ return TYPE; }
until|UNTIL	{ return UNTIL; }
var|VAR		{ return VAR; }
while|WHILE	{ return WHILE; }
write|WRITE	{ return WRITE; }

0{OCTAL}*	{ yylval.ival = stoi(yytext, 0 , 0); return INTEGER; }

0(x|X){HEX}*	{ yylval.ival = stoi(yytext, 0 , 0); return INTEGER; }

{DIGIT}*	{ yylval.ival = stoi(yytext); return INTEGER; }

{ID}	{ yylval.text = strdup(yytext); return IDENTIFIER; }

'.'	{ yylval.cval = yytext[1]; return CCONST; }
'\\n'	{ yylval.cval = '\n'; return CCONST; }
'\\r' 	{ yylval.cval = '\r'; return CCONST; }
'\\t'	{ yylval.cval = '\t'; return CCONST; }
'\\.'	{ yylval.cval = yytext[2]; return CCONST; }

"\""[^\"]*"\""	{ yylval.text = strdup(yytext); return STRING; }


"$".*	{ cout << "Found comment " << yytext << " on line " << lineNum << endl; }
[ \t\r] //Do nothing on whitespace

.	cout << "Unrecognized character " << yytext << " on line " << lineNum << endl;

%%
