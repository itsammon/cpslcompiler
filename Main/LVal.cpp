#include "LVal.hpp"
#include "Register.hpp"
#include "SymbolTable.hpp"
#include "Symbol.hpp"
#include "LiteralExpression.hpp"
#include "AddExpression.hpp"
#include "SubExpression.hpp"
#include "MultExpression.hpp"

#include <fstream>
#include <iostream>
#include <exception>

extern std::ofstream oFile;
extern int lineNum;

namespace Clegg5300
{

unique_ptr<Register> IdAccess::toRegister()
{
	auto r = Register::allocateRegister();	
	oFile << "#load in value for " << id << endl;
	oFile << "\tlw " << *r << ", " << getLocation() << endl;
	return r;
}

/*
Returns the location of the value in memory
*/
string IdAccess::getLocation(int offsetMod)
{
	string loc;
	auto memLoc = getMemLocation();
	switch (memLoc)
	{
		case GLOBAL:
			loc = to_string(getOffset() + offsetMod);
			loc += "($gp)";
		break;
		case STACK:
			// Stack is negative from the frame pointer
			loc = "-";
			loc += to_string(getOffset() + offsetMod);
			loc += "($fp)";
		break;
		case FRAME:
			// Used for parameters
			loc = to_string(getOffset() + offsetMod);
			loc += "($fp)";
		break;
		default:
		cout << "Undefined variable " << id << endl;
		throw exception();
	}
	return loc;
}

std::shared_ptr<Type> IdAccess::getType()
{
	if (isConst())
	{
		auto constant = table->lookupConst(id);
		if (constant != nullptr)
		{
			return constant->getType();
		}
		else
		{
			cout << "Undefined variable " << id << endl;
			throw exception();
		}
	}
	else
	{
		auto var = table->lookupVariable(id);
		if (var != nullptr)
		{
			return var->getType();
		}
		else
		{
			cout << "Undefined variable " << id << endl;
			throw exception();
		}
	}
}

int IdAccess::getValue()
{
	// emit the code to allow the expression to be processed
	auto constant = table->lookupConst(id);
	constant->emit();
	return constant->getValue();
}

bool IdAccess::isConst()
{
	return (table->lookupConst(id) != nullptr);
}

int IdAccess::getOffset()
{
	auto var = table->lookupVariable(id);
	if (var != nullptr)
	{
		return var->getOffset();
	}
	else
	{
		return -1;
	}
}

MemoryLocation IdAccess::getMemLocation()
{
	auto var = table->lookupVariable(id);
	if (var != nullptr)
	{
		return var->getLocation();
	}
	return GLOBAL;
}

shared_ptr<Expression> IdAccess::getAddress()
{
	cout << "Getting variable " << id << endl;
	auto var = table->lookupVariable(id);
	if (!var)
	{
		cout << "Undefined variable " << id << endl;
		throw exception();
	}
	auto loc = var->getLocation();
	auto offset = var->getOffset();
	
	if (loc != GLOBAL)
		offset += var->getType()->getSize();

	auto address = make_shared<MemoryAccessExpression>(loc, offset);
	
	if (dynamic_pointer_cast<ReferenceType>(var->getType()))
		return make_shared<MemoryAccessExpression>(address);
	return address;
}

///// ArrayAccess
std::unique_ptr<Register> ArrayAccess::toRegister()
{
	auto r = Register::allocateRegister();	
	oFile << "#load in value for " << endl;
	oFile << "\tlw " << *r << ", " << getLocation() << endl;
	return r;
}

/*
Returns the location of the value in memory
*/
string ArrayAccess::getLocation(int offsetMod)
{
	string loc;
	auto memLoc = getMemLocation();
	switch (memLoc)
	{
		case GLOBAL:
			loc = to_string(getOffset() + offsetMod);
			loc += "($gp)";
		break;
		case STACK:
			// Stack is negative from the frame pointer
			loc = "-";
			loc += to_string(getOffset() + offsetMod);
			loc += "($fp)";
		break;
		case FRAME:
			// Used for parameters
			loc = to_string(getOffset() + offsetMod);
			loc += "($fp)";
		break;
		default:
		cout << "Undefined array " << endl;
		throw exception();
	}
	return loc;
}


int ArrayAccess::getOffset()
{
	auto baseOffset = base->getOffset();
	if (baseOffset == -1)
		cout << "Unable to load base address" << endl;
	auto offset = baseOffset;
	return offset;
}

shared_ptr<Expression> ArrayAccess::getAddress()
{
	auto baseAddress = base->getAddress();
	auto type = getAType();
	
	if (exp->isConst())
	{
		int off = type->getType()->getSize() * (exp->getValue() - type->getStartIndex());
		if (off)
		{
			auto offset = make_shared<LiteralExpression>(off);
			return make_shared<AddExpression>(baseAddress, offset);
		}
		else
		{
			return baseAddress;
		}
	}
	else
	{
		auto lowerBound = make_shared<LiteralExpression>(type->getStartIndex());
		auto index = make_shared<SubExpression>(exp, lowerBound);
		auto size = make_shared<LiteralExpression>(type->getType()->getSize());
		auto offset = make_shared<MultExpression>(size, index);
		return make_shared<AddExpression>(baseAddress, offset);
	}
}


///// MemberAccess
std::unique_ptr<Register> MemberAccess::toRegister()
{
	auto r = Register::allocateRegister();	
	oFile << "#load in value for " << field << endl;
	oFile << "\tlw " << *r << ", " << getLocation() << endl;
	return r;
}

/*
Returns the location of the value in memory
*/
string MemberAccess::getLocation(int offsetMod)
{
	string loc;
	auto memLoc = getMemLocation();
	switch (memLoc)
	{
		case GLOBAL:
			loc = to_string(getOffset() + offsetMod);
			loc += "($gp)";
		break;
		case STACK:
			// Stack is negative from the frame pointer
			loc = "-";
			loc += to_string(getOffset() + offsetMod);
			loc += "($fp)";
		break;
		case FRAME:
			// Used for parameters
			loc = to_string(getOffset() + offsetMod);
			loc += "($fp)";
		break;
		default:
		cout << "Undefined variable " << field << endl;
		throw exception();
	}
	return loc;
}


int MemberAccess::getOffset()
{
	cout << "Getting offset" << endl;
	return getBType()->getOffset(field);
}

shared_ptr<Expression> MemberAccess::getAddress()
{
	cout << "In MemberAccess::getAddress(" << field << ")" << endl;
	
	auto baseaddress = base->getAddress();
	cout << "Got address!" << endl;
	if (baseaddress == nullptr)
	{
		cout << "Unable to load base address " << field << endl;
		throw exception();
	}
	cout << "Making literal expression for offset" << endl;
	auto offset = make_shared<LiteralExpression>(getOffset());
	return make_shared<AddExpression>(baseaddress, offset);
}

} // End Namespace
