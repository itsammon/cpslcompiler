#ifndef SYMBOL_HPP
#define SYMBOL_HPP

#include <string>
#include <memory>
#include "Type.hpp"
using namespace std;

namespace Clegg5300
{
/*
Create an enumerated type to hold the three possible locations
*/
enum MemoryLocation
{
	GLOBAL,
	STACK,
	FRAME
};
	
/*
Symbol class to hold information about a symbol
*/
class Symbol
{
	public:
		/* Constructor that adds all items to the symbol */
		Symbol(string id, std::shared_ptr<Type> type, MemoryLocation memLocation, int memOffset) : memoryLocation(memLocation), memoryOffset(memOffset), type(type), id(id) {}
		MemoryLocation getLocation();
		int getOffset();
		std::shared_ptr<Type> getType() { return type; }
		std::string getId() { return id; }
		
	private:
		int memoryOffset = 0;
		MemoryLocation memoryLocation;
		std::shared_ptr<Type> type;
		string id;
};

}

#endif
