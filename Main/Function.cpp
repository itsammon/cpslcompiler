#include "Function.hpp"
#include <string>

namespace Clegg5300
{
	bool FunctionSignature::operator==(const FunctionSignature& second) const
	{
		if (name != second.name)
			return false;
		if (parameters.size() != second.parameters.size())
			return false;
		// Compare types of function signature for equality.
		for (auto i = 0; i < parameters.size(); ++i)
		{
			// Need to modify to account for Pass by Reference
			// Get the second part of each pair
			auto leftType = std::dynamic_pointer_cast<ReferenceType>(parameters[i].second);
			auto rightType = std::dynamic_pointer_cast<ReferenceType>(second.parameters[i].second);
			auto lhs = leftType != nullptr ? leftType->getType() : parameters[i].second; // Set to nullptr
			auto rhs = rightType != nullptr ? rightType->getType() : second.parameters[i].second; // Set to nullptr
			if (lhs != rhs)
				return false;
		}
		return true;
	}

	// Need to order the function signatures in the map
	bool FunctionSignature::operator<(const FunctionSignature& second) const
	{
		if (name < second.name) return true;
		if (name > second.name) return false;
		
		if (rType < second.rType) return true;
		if (rType > second.rType) return false;
	
		if (parameters.size() < second.parameters.size()) return true;
		if (parameters.size() > second.parameters.size()) return false;
		
		for (auto i = 0; i < parameters.size(); ++i)
		{
			if (parameters[i].first < second.parameters[i].first) return true;
			if (parameters[i].first > second.parameters[i].first) return false;

			if (parameters[i].second < second.parameters[i].second) return true;
			if (parameters[i].second < second.parameters[i].second) return false;
		}

		return false;
	}

	std::string FunctionSignature::getLabel() const
	{
		if (label == -1)
		{
			label = FunctionSignature::getNextLabel();
		}
		std::string strLabel = std::string("F");
		strLabel += std::to_string(label);
		return strLabel;
	}
	
	int FunctionSignature::getNextLabel()
	{
		static int curLabel = 0;
		return ++curLabel;
	}	

	void Function::emit()
	{
		// TODO add emit code here
		
		// Emit the prologue
		oFile << endl;
		oFile << "# Function " << signature->getName() << endl;
		// Allocate space for local variables
		// First local variable is the return type
		oFile << signature->getLabel() << ": #Allocate space for variables" << endl;
		oFile << "\taddi $sp, $sp, -";
		oFile << table->getMemOffset() << endl;
		
		body->emit();
		
		oFile << "# epilogue" << endl;
		oFile << signature->getLabel() << "_epi: addi $sp, $sp, ";
		oFile << table->getMemOffset() << endl;
		oFile << "\tjr $ra" << endl;
		oFile << endl;
	}
}
