#include "Symbol.hpp"

namespace Clegg5300
{

MemoryLocation Symbol::getLocation()
{
	return memoryLocation;
}

int Symbol::getOffset()
{
	return memoryOffset;
}

}
