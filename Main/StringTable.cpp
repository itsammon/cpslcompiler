#include "StringTable.hpp"

namespace Clegg5300
{
void StringTable::printList()
{
	oFile << ".data" << endl;
	for (int i = 0; i < stringList.size(); ++i)
	{
		oFile << "SL" << i << ": .asciiz " << stringList[i] << endl;
	}
}

int StringTable::insert(string value)
{
	int label;
	stringList.push_back(value);
	label = currentNumber;
	++currentNumber;
	return label;
}
}
