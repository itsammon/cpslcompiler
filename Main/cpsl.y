%{
  #include <iostream>
  #include <fstream>
  #include <string>
  #include "Front.hpp"
  #include "StringTable.hpp"
  using namespace std;
  using namespace Clegg5300;

  // Stuff from flex used by bison
  extern "C" int yylex();
  extern "C" int yyparse();
  extern "C" FILE *yyin;
  extern int lineNum;

  extern StringTable stringTable;

  // Declare yyerror here
  void yyerror(const char *);
%}

%define parse.error verbose

%union {
	int ival;
	char* text;
	char cval;
}

%token ASSIGN ":="
%token LESSOREQUAL "<="
%token GREATEROREQUAL ">="
%token EQUAL "<>"

%token ARRAY
%token START
%token CHR
%token CONST
%token DO
%token DOWNTO
%token ELSE
%token ELSEIF
%token END
%token FOR
%token FORWARD
%token FUNCTION
%token IF
%token OF
%token ORD
%token PRED
%token PROCEDURE
%token READ
%token RECORD
%token REF
%token REPEAT
%token RETURN
%token STOP
%token SUCC
%token THEN
%token TO
%token TYPE
%token UNTIL
%token VAR
%token WHILE
%token WRITE

%token <text> IDENTIFIER

%token <ival> INTEGER
%token <text> STRING
%token <cval> CCONST

%left '|'
%left '&'
%nonassoc '~'
%nonassoc '=' "<>" '<' "<=" '>' ">="
%left '-' '+'
%left '*' '/' '%'
%nonassoc UNEG

%type <ival> proceduredec
%type <ival> psignature
%type <ival> functiondec
%type <ival> fsignature
%type <ival> formalparam
%type <ival> varref
%type <ival> body
%type <ival> block

%type <ival> statementseq
%type <ival> statement

%type <ival> functioncall

%type <ival> recordlist
%type <ival> recordfields

%type <ival> arraytype
%type <ival> recordtype
%type <ival> simpletype
%type <ival> type
%type <ival> identlist
%type <ival> assignment
%type <ival> forstatement
%type <ival> forhead
%type <ival> tohead
%type <ival> ifstatement
%type <ival> elseifstatement
%type <ival> elsestatement
%type <ival> stopstatement
%type <ival> procedurecall
%type <ival> procedureargs
%type <ival> readstatement
%type <ival> repeatstatement
%type <ival> returnstatement
%type <ival> writestatement
%type <ival> whilestatement
%type <ival> lvallist
%type <ival> writelist
%type <ival> exp
%type <ival> lval

%%
program: programhead block '.' { endProgram($2); }
	;

programhead: %empty
	| constdec 
	| constdec typedec
	| constdec typedec vardec
	| constdec typedec vardec functiondef
	| constdec vardec
	| constdec vardec functiondef
	| constdec functiondef
	| typedec
	| typedec vardec
	| typedec vardec functiondef
	| vardec
	| vardec functiondef
	| functiondef
	;

functiondef: functiondef proceduredec
	| functiondef functiondec
	| proceduredec
	| functiondec
	;

constdec: CONST constlist
	;

constlist: IDENTIFIER '=' exp ';' { addConst($1, $3); }
	| constlist IDENTIFIER '=' exp ';' { addConst($2, $4); }
	;

proceduredec: psignature ';' FORWARD ';' { $$ = addFunction($1); }
	| psignature ';' body ';' { $$ = addFunction($1, $3); }
	;

psignature: PROCEDURE IDENTIFIER '(' formalparam ')' { $$ = addSignature($2,$4, -1); }
	;

functiondec: fsignature ';' FORWARD ';' { $$ = addFunction($1); }
	| fsignature ';' body ';' { $$ = addFunction($1, $3); }
	;

fsignature: FUNCTION IDENTIFIER '(' formalparam ')' ':' type { $$ = addSignature($2, $4, $7); }
	;

formalparam: %empty { $$ = -1; }
	| varref identlist ':' type { $$ = addParameters($1, $2, $4); }
	| formalparam ';' varref identlist ':' type { $$ = addParameters($3, $4, $6); }
	;

varref: VAR { $$ = 0; }
	| REF { $$ = 1; }
	| %empty { $$ = 0; }
	;

body: block { $$ = $1; }
	| constdec block { $$ = $2; }
	| constdec typedec block { $$ = $3; }
	| constdec typedec vardec block { $$ = $4; }
	| constdec vardec block { $$ = $3; }
	| typedec block { $$ = $2; }
	| typedec vardec block { $$ = $3; }
	| vardec block { $$ = $2; }
	;

block: START statementseq END { $$ = $2; }
	;

typedec: TYPE IDENTIFIER '=' type ';' { addType($2, $4); }
	| typedec IDENTIFIER '=' type ';' { addType($2, $4); }
	;

type: simpletype { $$ = $1; }
	| recordtype { $$ = $1; }
	| arraytype { $$ = $1; }
	;

simpletype: IDENTIFIER { $$ = lookupType($1); }
	;

recordtype: RECORD recordlist END { $$ = createRecordType($2); }
	;

recordlist: %empty { $$ = -1; }
	| recordlist recordfields { $$ = addRecordList($1, $2); }
	;

recordfields: identlist ':' type ';' { $$ = addField($1, $3); }
	;

arraytype: ARRAY '[' exp ':' exp ']' OF type { $$ = addArrayType($3, $5, $8); }
	;

identlist: IDENTIFIER { $$ = addIdent(-1, $1); }
	| identlist ',' IDENTIFIER { $$ = addIdent($1, $3); }
	;

vardec: VAR identlist ':' type ';' { addVariables($2, $4); }
	| vardec identlist ':' type ';' { addVariables($2, $4); }
	;

statementseq: statement { $$ = addStatement($1); }
	| statementseq ';' statement { $$ = addStatement($1, $3); }
	;

statement: assignment { $$ = $1; }
	| ifstatement { $$ = $1; }
	| whilestatement { $$ = $1; }
	| repeatstatement { $$ = $1; }
	| forstatement { $$ = $1; }
	| stopstatement { $$ = $1; }
	| returnstatement { $$ = $1; }
	| readstatement { $$ = $1; }
	| writestatement { $$ = $1; }
	| procedurecall { $$ = $1; }
	| nullstatement { $$ = -1; }
	;

assignment: lval ":=" exp { $$ = assignStatement($1, $3); }
	;

ifstatement: IF exp THEN statementseq elseifstatement END { $$ = ifStatement($2, $4, $5, -1); }
	| IF exp THEN statementseq elseifstatement elsestatement END { $$ = ifStatement($2, $4, $5, $6); }
	;

elseifstatement: ELSEIF exp THEN statementseq { $$ = elseIfStatement($2, $4); }
	| elseifstatement ELSEIF exp THEN statementseq { $$ = addElseIf($1, $3, $5); }
	| %empty { $$ = -1; }
	;

elsestatement: ELSE statementseq { $$ = elseStatement($2); }
	;

whilestatement: WHILE exp DO statementseq END { $$ = addWhile($2, $4); }
	;

repeatstatement: REPEAT statementseq UNTIL exp { $$ = addRepeat($2, $4); }
	;

forstatement: FOR forhead tohead DO statementseq END { $$ = forStatement($2, $3, $5); }
	;

forhead: IDENTIFIER ":=" exp { $$ = addForHead($1, $3); }
	;

tohead: TO exp { $$ = addToHead(1, $2); }
	| DOWNTO exp { $$ = addToHead(-1, $2); }
	;

stopstatement: STOP	{ $$ = stopStatement(); }
	;

returnstatement: RETURN exp { $$ = addReturn($2); }
	| RETURN { $$ = addReturn(-1); }
	;

readstatement: READ '(' lvallist ')' { $$ = $3; }
	;

lvallist: lval { $$ = addRead($1); }
	| lvallist ',' lval { $$ = addRead($1, $3); }
	;

writestatement: WRITE '(' writelist ')' { $$ = $3; }
	;

procedurecall: IDENTIFIER '(' ')' { $$ = addProcCall($1, -1); }
	| IDENTIFIER '(' procedureargs ')' { $$ = addProcCall($1, $3); }

writelist: exp { $$ = addWrite($1); }
	| writelist ',' exp { $$ = addWrite($1,$3); }
	;

procedureargs: exp { $$ = addArgs(-1, $1); }
	| procedureargs ',' exp { $$ = addArgs($1, $3); }
	;

nullstatement: %empty
	;

exp:	exp '|' exp { $$ = addOr($1, $3); }
	| exp '&' exp { $$ = addAnd($1, $3); }
	| exp '=' exp { $$ = addEquals($1, $3); }
	| exp "<>" exp { $$ = addNotEqual($1, $3); }
	| exp "<=" exp { $$ = addLessOrEqual($1, $3); }
	| exp ">=" exp { $$ = addGreaterOrEqual($1, $3); }
	| exp '<' exp { $$ = addLessThan($1, $3); }
	| exp '>' exp { $$ = addGreaterThan($1, $3); }
	| exp '+' exp { $$ = addAdd($1, $3); }
	| exp '-' exp { $$ = addSub($1, $3); }
	| exp '*' exp { $$ = addMult($1, $3); }
	| exp '/' exp { $$ = addDivide($1, $3); }
	| exp '%' exp { $$ = addMod($1, $3); }
	| '~' exp { $$ = addNot($2); }
	| '-' exp %prec UNEG { $$ = addNeg($2); }
	| '(' exp ')' { $$ = $2; }
	| functioncall { $$ = $1; }
	| CHR '(' exp ')' { $$ = addChr($3); }
	| ORD '(' exp ')' { $$ = addOrd($3); }
	| PRED '(' exp ')' { $$ = addPred($3); }
	| SUCC '(' exp ')' { $$ = addSucc($3); }
	| lval { $$ = loadLVal($1);}
	| STRING { $$ = addString($1); }
	| INTEGER { $$ = addInteger($1); }
	| CCONST { $$ = addCharLit($1); }
	;
	
functioncall: IDENTIFIER '(' procedureargs ')' { $$ = addFunctCall($1, $3); }
	| IDENTIFIER '(' ')' { $$ = addFunctCall($1, -1); }
	;

lval:	IDENTIFIER { $$ = addLVal($1); }
	| lval '.' IDENTIFIER { $$ = addMemberAccess($1, $3); }
	| lval '[' exp ']' { $$ = addArrayAccess($1, $3); }
	;

%%

//Display the error message and line number on an error
void yyerror(const char *s)
{
	cout << "Parse error on line " << lineNum << "! Message: " << s << endl;
	exit(-1);
}
