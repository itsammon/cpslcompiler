#ifndef FRONT_HPP
#define FRONT_HPP

#include "SymbolTable.hpp"
#include "Type.hpp"
#include <memory>

namespace Clegg5300
{
void parseCPSL(std::string iFile, std::string outFile);
void setup();
void endOutput();
void endProgram(int seqIndex);

int addArrayType(int e1, int e2, int typeIndex);

int createRecordType(int fieldsIndex);
int addRecordList(int recordList, int fieldList);
int addField(int identIndex, int typeIndex);

int addSignature(char* ident, int params, int t);
int addParameters(int byRef, int idList, int typeIndex);
int addFunction(int sig);
int addFunction(int sig, int sequence);

int addProcCall(char* ident, int argsIndex);
int addFunctCall(char* ident, int argsIndex);
int addArgs(int argList, int e);
int addReturn(int e);

int addStatement(int statementIndex);
int addStatement(int seqIndex, int statementIndex);
void emitSequence(int s);

void emitStatement(int s);
int assignStatement(int lv, int e);
int forStatement(int h, int t, int seq);
int addForHead(char* ident, int e);
int addToHead(int inc, int e);
int addRead(int lv);
int addRead(int s, int lv);
int addRepeat(int seq, int e);
int ifStatement(int e, int s, int eiState, int eState);
int elseIfStatement(int e, int seq);
int addElseIf(int elseIf, int e, int seq);
int elseStatement(int seqIndex);
int stopStatement();
int addWrite(int e);
int addWrite(int s, int e);
int addWhile(int e, int seq);

int addAdd(int f, int s);
int addAnd(int f, int s);
int addChr(int e);
int addDivide(int f, int s);
int addEquals(int f, int s);
int addGreaterOrEqual(int f, int s);
int addGreaterThan(int f, int s);
int addLessOrEqual(int f, int s);
int addLessThan(int f, int s);
int addMod(int f, int s);
int addMult(int f, int s);
int addNeg(int e);
int addNot(int e);
int addNotEqual(int f, int s);
int addOr(int f, int s);
int addOrd(int e);
int addPred(int e);
int addSub(int f, int s);
int addSucc(int e);

int addInteger(int i);
int addCharLit(char c);
int addString(char* s);


int addIdent(int listIndex, char* identifier);
void addType(char* identifier, int type);
int loadLVal(int id);
int lookupType(char* identifier);
int addLVal(char* identifier);
int addMemberAccess(int lv, char* identifier);
int addArrayAccess(int lv, int e);

void addConst(char* identifier, int exp);
void addVariables(int listIndex, int type);
void addVariables(int listIndex, shared_ptr<Type> type);

}

#endif
