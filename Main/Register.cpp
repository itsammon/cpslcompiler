#include "Register.hpp"
#include <memory>
#include <vector>
#include <string>
#include <iostream>
using namespace std;

std::vector<std::string> Register::pool;
std::vector<std::string> Register::inUse;

void Register::init()
{
	pool.push_back("$s0");
	pool.push_back("$s1");
	pool.push_back("$s2");
	pool.push_back("$s3");
	pool.push_back("$s4");
	pool.push_back("$s5");
	pool.push_back("$s6");
	pool.push_back("$s7");

	pool.push_back("$t0");
	pool.push_back("$t1");
	pool.push_back("$t2");
	pool.push_back("$t3");
	pool.push_back("$t4");
	pool.push_back("$t5");
	pool.push_back("$t6");
	pool.push_back("$t7");
	pool.push_back("$t8");
	pool.push_back("$t9");
}

std::unique_ptr<Register> Register::allocateRegister()
{
	auto n = Register::pool.back();
	pool.pop_back();
	inUse.push_back(n);
	std::unique_ptr<Register> r(new Register(n));
	return r;
}

std::ostream& operator<< (std::ostream& out, Register const & r)
{
	out << r.name;
}
