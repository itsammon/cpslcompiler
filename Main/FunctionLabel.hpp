#ifndef FUNCTION_LABEL_HPP
#define FUNCTION_LABEL_HPP

#include <string>
#include <iostream>

class FunctionLabel
{
	public:
		static std::string getNewLabel()
		{
			std::string label = std::string("F");
			label += std::to_string(numLabel);
			numLabel++;
			return label;
		};
		static std::string getCurrentLabel()
		{
			std::string label = std::string("F");
			label += std::to_string(numLabel);
			return label;
		};
	private:
		static int numLabel;
};


#endif
