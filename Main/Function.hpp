#ifndef FUNCTION_HPP
#define FUNCTION_HPP

#include <memory>
#include <string>
#include <vector>
#include "FunctionLabel.hpp"
#include "StatementSequence.hpp"
#include "Type.hpp"
#include "SymbolTable.hpp"
using namespace std;

namespace Clegg5300
{
class FunctionSignature
{
	public:
		FunctionSignature(std::string n, std::vector<std::pair<std::string, std::shared_ptr<Type>>> p,
		std::shared_ptr<Type> r) : 
			name(n), parameters(p), rType(r), label(-1)
			{}
		
		shared_ptr<Type> getReturnType() { return rType; }
		string getLabel() const;
		string getName() { return name; }
		std::vector<std::pair<std::string, std::shared_ptr<Type>>> getParameters() { return parameters; }
		// Function to enable easy comparison
		bool operator ==(const FunctionSignature &) const;
		bool operator<(const FunctionSignature&) const;
	private:
		// Parameter list
		std::vector<std::pair<std::string, std::shared_ptr<Type>>> parameters;
		std::shared_ptr<Type> rType;
		std::string name;
		static int getNextLabel();
		mutable int label;
};


class Function
{
	public:
		Function(std::shared_ptr<FunctionSignature> s,
			std::shared_ptr<StatementSequence> b,
			std::shared_ptr<SymbolTable> t)
			: signature(s), body(b), table(t) {}
		void emit();
		std::shared_ptr<FunctionSignature> getSignature()
			{ return signature; }
		std::shared_ptr<SymbolTable> getTable() { return table; }
	private:
		std::shared_ptr<SymbolTable> table;
		std::shared_ptr<StatementSequence> body;
		std::shared_ptr<FunctionSignature> signature;
};
}

#endif
