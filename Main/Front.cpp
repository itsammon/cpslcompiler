#include "Front.hpp"
#include <memory>
#include <vector>
#include <iostream>
#include <cassert>
#include <fstream>

#include "StringTable.hpp"
#include "SymbolTable.hpp"
#include "Type.hpp"
#include "LVal.hpp"

// Expressions
#include "AddExpression.hpp"
#include "AndExpression.hpp"
#include "ChrExpression.hpp"
#include "DivideExpression.hpp"
#include "EqualsExpression.hpp"
#include "Expression.hpp"
#include "FunctionCall.hpp"
#include "GreaterOrEqualExpression.hpp"
#include "GreaterThanExpression.hpp"
#include "LessOrEqualExpression.hpp"
#include "LessThanExpression.hpp"
#include "LiteralExpression.hpp"
#include "LoadExpression.hpp"
#include "ModExpression.hpp"
#include "MultExpression.hpp"
#include "NegExpression.hpp"
#include "NotExpression.hpp"
#include "NotEqualExpression.hpp"
#include "OrExpression.hpp"
#include "OrdExpression.hpp"
#include "PredExpression.hpp"
#include "StringExpression.hpp"
#include "SubExpression.hpp"
#include "SuccExpression.hpp"

// Statements
#include "AssignStatement.hpp"
#include "ForStatement.hpp"
#include "ForHead.hpp"
#include "ToHead.hpp"
#include "IfStatement.hpp"
#include "ElseIfStatement.hpp"
#include "ElseStatement.hpp"
#include "ProcedureCall.hpp"
#include "ReadStatement.hpp"
#include "RepeatStatement.hpp"
#include "ReturnStatement.hpp"
#include "Statement.hpp"
#include "StopStatement.hpp"
#include "WriteStatement.hpp"
#include "WhileStatement.hpp"

// Functions
#include "Function.hpp"
using namespace std;

// Forward Declarations
// Stuff from flex used by bison
extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern int lineNum;

extern ofstream oFile;
extern Clegg5300::StringTable stringTable;

namespace Clegg5300
{

/*
Creates the predefined symbol table
*/
shared_ptr<SymbolTable> getPredefinedTable()
{
	// Make table
	auto table = make_shared<SymbolTable>(nullptr, GLOBAL);
	// Prefill table with predefined types
	table->addType("boolean", BuiltInType::getBool());
	table->addType("BOOLEAN", BuiltInType::getBool());
	table->addType("integer", BuiltInType::getInt());
	table->addType("INTEGER", BuiltInType::getInt());
	table->addType("char", BuiltInType::getChar());
	table->addType("CHAR", BuiltInType::getChar());
	table->addType("string", BuiltInType::getString());
	table->addType("STRING", BuiltInType::getString());
	
	// Add in boolean constants later
	table->addConst("true", make_shared<LiteralExpression>(true));
	table->addConst("TRUE", make_shared<LiteralExpression>(true));
	table->addConst("false", make_shared<LiteralExpression>(false));
	table->addConst("FALSE", make_shared<LiteralExpression>(false));

	return table;
}

template<typename T> class SyntaxList
{
	public:
		SyntaxList() : source() {}
		// Get an element from the list
		shared_ptr<T> get(int index)
		{
			// If the index does not exist return null
			if (index < 0) return nullptr;
			// If we have valid index return the source index
			if (index < (source.size() + 1))
			{
				return source[index];
			}
			// If the index was larger than the source return null
			return nullptr;
		}
		// Add an element to the list
		int add(shared_ptr<T> element)
		{
			// Save the index we are about to insert into
			auto index = source.size();
			source.push_back(element);
			return index;
		}
		// Allow clearing of data
		void clear() {source.clear();}
	private:
		// Place to store the list
		vector<shared_ptr<T>> source;
};

/*
Keep track of the current scope we are in while parsing
*/
class State
{
	public:
	// Make a main instance
	static std::shared_ptr<State> instance()
	{
		if (!mainInstance) 
		{
			mainInstance = make_shared<State>();
		}
		return mainInstance;
	}
	SyntaxList<vector<string>> idLists;
	SyntaxList<vector<pair<string,shared_ptr<Type>>>> params;
	SyntaxList<vector<shared_ptr<Expression>>> argLists;
	SyntaxList<Type> typeList;
	SyntaxList<LVal> lvals;
	SyntaxList<Expression> expressions;
	SyntaxList<Statement> statements;
	SyntaxList<StatementSequence> statementSeqs;
	SyntaxList<ForHead> forHeads;
	SyntaxList<ToHead> toHeads;
	SyntaxList<FunctionSignature> functionSigs;
	// Map signatures to their functions
	map<FunctionSignature, shared_ptr<Function>> functions;

	// Initialize the values of the first symbolTable
	void initialize()
	{
		currentSymbolTable = getPredefinedTable();
	}

	shared_ptr<SymbolTable> getSymbolTable()
	{
		assert(currentSymbolTable);
		return currentSymbolTable;
	}

	void pushSymbolTable(int offset = 0)
	{
		currentSymbolTable = std::make_shared<SymbolTable>(currentSymbolTable, STACK, offset);
	}

	void popSymbolTable()
	{
		currentSymbolTable = currentSymbolTable->getParent();
	}

	private:
		static shared_ptr<State> mainInstance;
		shared_ptr<SymbolTable> currentSymbolTable;
};

// Declaration to allowing linking
std::shared_ptr<State> Clegg5300::State::mainInstance;

/*
Parses the file given and puts it's output into the outfile
*/
void parseCPSL(std::string iFile, std::string outFile)
{
	yyin = fopen(iFile.c_str(), "r");

	// Initialize register list
	Register::init();

	// Open the file for writing
	oFile.open(outFile, std::ios_base::out);

	setup();
	
	State::instance()->initialize();
	do
	{
		yyparse();
	}
	while(!feof(yyin));

	stringTable.printList();
	endOutput();

	cout << "Finished parsing the file!" << endl;

	fclose(yyin);
	oFile.close();
}

//// CODE OUTPUT STUFF ///////
/*
Does initial setup in the outfile
*/
void setup()
{
	oFile << ".globl main" << endl;
	oFile << ".text" << endl;
	oFile << "main: la $gp, GA" << endl;
	oFile << "\tori $fp, $sp, 0" << endl;
	oFile << "\t j prog" << endl;
}

/*
Finishes up output for file
*/
void endOutput()
{
	// TODO Change offset stuff to only do the global variables
	oFile << "GA: " << endl;
	auto table = State::instance()->getSymbolTable();
	oFile << ".align 2" << endl;
	oFile << ".space " << table->getMemOffset() * 4 << endl;
}

/*
Outputs a syscall to end the program
*/
void endProgram(int seqIndex)
{
	oFile << "prog: " <<endl;
	emitSequence(seqIndex);
	oFile << "prog_epi: " << endl;
	oFile << "#End the program" << endl;
	oFile << "\tli $v0, 10" << endl;
	oFile << "\tsyscall" << endl;
}

///// USER DEFINED TYPE SECTION
int addArrayType(int e1, int e2, int typeIndex)
{
	cout << "Creating array type" << endl;
	auto state = State::instance();
	auto table = state->getSymbolTable();
	auto type = state->typeList.get(typeIndex);
	auto exp1 = state->expressions.get(e1);
	auto exp2 = state->expressions.get(e2);
	int lower = -1;
	int upper = -1;
	
	// Check for valid array bits
	if (!type) { return -1; cout << "No Type for array found!" << endl; }
	if (!exp1) return -1;
	if (!exp2) return -1;
	// Array's indexes need to be constant expressions
	if (!exp1->isConst()) { return -1; cout << "Non constant expression for array found!" << endl; }
	if (!exp2->isConst()) { return -1; cout << "Non constant expression for array found!" << endl; }
	
	// Get the actual start and end values
	lower = exp1->getValue();
	upper = exp2->getValue();
	auto array = std::make_shared<ArrayType>(type, lower, upper);
	return state->typeList.add(array);
}

namespace
{
std::map<std::string, shared_ptr<Type>> currentRecordFields;
}

int createRecordType(int fieldsIndex)
{
	cout << "Creating record type" << endl;
	return fieldsIndex;
}

int addRecordList(int recordList, int fieldList)
{
	cout << "Creating record field list" << endl;
	auto state = State::instance();
	if (recordList == -1)
	{
		auto record = std::make_shared<RecordType>();
		for (auto &iter : currentRecordFields)
		{
			record->addMember(iter.first, iter.second);
		}
		return state->typeList.add(record);
	}
	else
	{
		// Make sure the type is appropriate
		auto type = std::dynamic_pointer_cast<RecordType>(state->typeList.get(recordList));
		if (!type) return -1;
	
		auto fields = std::dynamic_pointer_cast<RecordType>(state->typeList.get(fieldList));
		if (!fields) return -1;
		for (auto && field : fields->getFields())
		{
			type->addMember(field.first, field.second);
		}
		return recordList;
	}
}

int addField(int identIndex, int typeIndex)
{
	auto state = State::instance();
	auto type = state->typeList.get(typeIndex);
	if (!type) return -1;
	auto idents = state->idLists.get(identIndex);
	auto newType = std::make_shared<RecordType>();
	for (auto && id: *idents)
	{
		newType->addMember(id, type);
		currentRecordFields[id] = type;
	}
	return state->typeList.add(newType);
}


///// FUNCTION SECTION
int addSignature(char* ident, int params, int t)
{
	auto state = State::instance();
	auto returnType = state->typeList.get(t); // if it is -1 then it is nullptr
	// Get the parameters
	auto parameters = state->params.get(params);
	std::vector<std::pair<std::string, std::shared_ptr<Type>>> args;
	// If there are parameters then we need to copy them over
	if (parameters)
	{
		for (auto&& param : *(parameters))
		{
			// Add to the symboltable
			state->getSymbolTable()->addParameter(param.first, param.second);
			args.push_back(param);
		}
	}
	// Add signature to the list
	auto signature = std::make_shared<FunctionSignature>(ident, args, returnType);
	// Free up character array
	delete(ident);
	// Add a symbol table in case there was a function body
	state->pushSymbolTable();

	// Add the return variable to the symbol table
	auto identList = addIdent(-1, "_return");
	addVariables(identList, returnType);

	return state->functionSigs.add(signature);
}

int addParameters(int byRef, int listIndex, int typeIndex)
{
	// Get the current state we are in
	auto state = State::instance();
	// Get the type from list of types
	auto type = state->typeList.get(typeIndex);
	// get the list of ids from the state
	auto idList = state->idLists.get(listIndex);
	// Check for byRef here

	// Create a list of parameters
	auto params = make_shared<std::vector<std::pair<std::string, std::shared_ptr<Type>>>>();

	for (auto && id : *idList)
	{
		// This is where we create the parameter list
		params->push_back(std::make_pair(id, type));
	}
	return state->params.add(params);
}

int addFunction(int sig)
{
	// Get the current state we are in
	auto state = State::instance();
	// Get the function signature
	auto signature = state->functionSigs.get(sig);
	// Check if there is an existing function otherwise we add it
	auto iterator = state->functions.find(*signature);
	if (iterator == state->functions.end())
	{
		state->functions[*signature] = nullptr;	
	}
	state->popSymbolTable();
}

int addFunction(int sig, int sequence)
{
	// Get the current state we are in
	auto state = State::instance();
	// Get the function signature
	auto signature = state->functionSigs.get(sig);
	// Get the body of the function
	auto seq = state->statementSeqs.get(sequence);
	// Add the parameters to the symboltable
	for (auto&& param: signature->getParameters())
	{
		state->getSymbolTable()->addParameter(param.first, param.second);
	}
	auto table = state->getSymbolTable();
	// Add the return statement at the end in case there isn't one
	auto rstatement = addReturn(-1);
	addStatement(sequence, rstatement);
	auto funct = std::make_shared<Function>(signature, seq, table);
	// if there is a function already there, replace the signature
	state->functions[*signature] = funct;
	table->setReturn(funct->getSignature()->getLabel());
	state->popSymbolTable();
	funct->emit();
}

int addArgs(int argList, int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	if (argList == -1)
	{
		// Create a list to be added to
		auto list = make_shared<vector<shared_ptr<Expression>>>();
		list->push_back(exp);
		return state->argLists.add(list);
	}
	else
	{
		// Add item to the end of the list of identifiers
		auto list = state->argLists.get(argList);
		list->push_back(exp);
		return argList;
	}
}

int addProcCall(char* ident, int argsIndex)
{
	auto state = State::instance();
	auto aArgs = state->argLists.get(argsIndex);
	std::vector<std::shared_ptr<Expression>> args;
	auto params = std::vector<pair<string, shared_ptr<Type>>>();
	if (aArgs)
	{
		for ( int i = 0; i < aArgs->size(); i++)
		{
			args.push_back(aArgs->at(i));
		}
		
		for (int i = 0; i < args.size(); ++i)
		{
			params.push_back(make_pair("", args[i]->getType()));
		}
	}
	
	auto sig = FunctionSignature(ident, params, nullptr);
	for (auto iter : state->functions)
	{
		if (iter.first == sig)
		{
			auto funct = iter.second;
			// Handle pass by ref


			return state->statements.add(make_shared<ProcedureCall>(funct, args));
		}
	}
	
	cout << "Unable to find function " << ident << endl;
	return -1;
}

int addFunctCall(char* ident, int argsIndex)
{
	auto state = State::instance();
	auto aArgs = state->argLists.get(argsIndex);
	std::vector<std::shared_ptr<Expression>> args;
	auto params = std::vector<pair<string, shared_ptr<Type>>>();
	if (aArgs)
	{
		for ( int i = 0; i < aArgs->size(); i++)
		{
			args.push_back(aArgs->at(i));
		}
		
		for (int i = 0; i < args.size(); ++i)
		{
			params.push_back(make_pair("", args[i]->getType()));
		}
	}
	
	auto sig = FunctionSignature(ident, params, nullptr);
	for (auto iter : state->functions)
	{
		if (iter.first == sig)
		{
			auto funct = iter.second;
			// Handle pass by ref


			return state->expressions.add(make_shared<FunctionCall>(funct, args));
		}
	}
	
	cout << "Unable to find function " << ident << endl;
	return -1;
}

int addReturn(int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	if (e == -1)
		exp = nullptr;
	auto table = state->getSymbolTable();
	auto statement = make_shared<ReturnStatement>(exp, table);
	return state->statements.add(statement);
	
}


///// STATEMENT SEQUENCE SECTION
/*
Adds a statement to the sequence
*/
int addStatement(int statementIndex)
{
	auto state = State::instance();
	auto sequence = make_shared<StatementSequence>();
	auto statement = state->statements.get(statementIndex);
	sequence->add(statement);
	return state->statementSeqs.add(sequence);
}

int addStatement(int seqIndex, int statementIndex)
{
	if (statementIndex != -1)
	{
		auto state = State::instance();
		auto sequence = state->statementSeqs.get(seqIndex);
		auto statement = state->statements.get(statementIndex);
		sequence->add(statement);
		return seqIndex;
	}
	else
		return seqIndex;
}

void emitSequence(int s)
{
	auto state = State::instance();
	auto sequence = state->statementSeqs.get(s);
	sequence->emit();
}

///// STATEMENTS SECTION
void emitStatement(int s)
{
	auto state = State::instance();
	auto statement = state->statements.get(s);
	statement->emit();
}

int assignStatement(int lv, int e)
{
	auto state = State::instance();
	auto lval = state->lvals.get(lv);
	auto exp = state->expressions.get(e);
	auto statement = make_shared<AssignStatement>(lval, exp);
	return state->statements.add(statement);

}

int forStatement(int h, int t, int seq)
{
	auto state = State::instance();
	auto head = state->forHeads.get(h);
	auto to = state->toHeads.get(t);
	auto sequence = state->statementSeqs.get(seq);
	auto statement = make_shared<ForStatement>(head, to, sequence);
	// Remove the table added by the forHead
	state->popSymbolTable();
	return state->statements.add(statement);
}

int addForHead(char* ident, int e)
{
	auto state = State::instance();
	auto oldTable = state->getSymbolTable();
	int currOffset;
	// Check if we are a local table or not
	if (oldTable->getMemLocation() != GLOBAL)
	{
		// Get the current memory offset for our new table
		currOffset = state->getSymbolTable()->getMemOffset();
	}
	else
	{
		currOffset = 0;
	}
	// Add a table with the memory offset
	state->pushSymbolTable(currOffset);
	
	// Add the identifier to the table
	auto exp = state->expressions.get(e);
	auto type = exp->getType();
	auto identList = addIdent(-1, ident);
	addVariables(identList, type);
	auto lValIndex = addLVal(ident);
	auto lval = state->lvals.get(lValIndex);
	auto head = make_shared<ForHead>(lval, exp);
	return state->forHeads.add(head);
}

int addToHead(int inc, int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto head = make_shared<ToHead>(inc, exp);
	return state->toHeads.add(head);
}

int ifStatement(int e, int s, int eiState, int eState)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto sequence = state->statementSeqs.get(s);
	shared_ptr<ElseIfStatement> eiStatement;
	shared_ptr<ElseStatement> eStatement;
	if (eiState != -1)
	{
		eiStatement = std::dynamic_pointer_cast<ElseIfStatement>(state->statements.get(eiState));
	}
	if (eState != -1)
	{
		eStatement = std::dynamic_pointer_cast<ElseStatement>(state->statements.get(eState));
	}		
	auto statement = make_shared<IfStatement>(exp, sequence, eiStatement, eStatement);
	return state->statements.add(statement);
}

int elseIfStatement(int e, int seq)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto sequence = state->statementSeqs.get(seq);
	auto statement = make_shared<ElseIfStatement>(exp, sequence);
	return state->statements.add(statement);
}

int addElseIf(int elseIf, int e, int seq)
{
	if (elseIf != -1)
	{
		auto state = State::instance();
		auto exp = state->expressions.get(e);
		auto sequence = state->statementSeqs.get(seq);
		auto elseState = std::dynamic_pointer_cast<ElseIfStatement>(state->statements.get(elseIf));
		auto statement = make_shared<ElseIfStatement>(exp, sequence);
		elseState->addElseIf(statement);
		return state->statements.add(statement);
	}
	else
		return elseIf;
}

int elseStatement(int seqIndex)
{
	auto state = State::instance();
	auto sequence = state->statementSeqs.get(seqIndex);
	auto statement = make_shared<ElseStatement>(sequence);
	return state->statements.add(statement);
}

int addRead(int lv)
{
	auto state = State::instance();
	auto lval = state->lvals.get(lv);
	auto statement = make_shared<ReadStatement>(lval);
	return state->statements.add(statement);
}

int addRead(int s, int lv)
{
	auto state = State::instance();
	auto lval = state->lvals.get(lv);
	auto statement = std::dynamic_pointer_cast<ReadStatement>(state->statements.get(s));
	statement->append(lval);
	return s;
}

int addRepeat(int seq, int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto sequence = state->statementSeqs.get(seq);
	auto statement = make_shared<RepeatStatement>(sequence, exp);
	return state->statements.add(statement);
}

int stopStatement()
{
	auto state = State::instance();
	auto statement = make_shared<StopStatement>();
	return state->statements.add(statement);
}

int addWrite(int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto statement = make_shared<WriteStatement>(exp);
	return state->statements.add(statement);
}

int addWrite(int s, int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto statement = std::dynamic_pointer_cast<WriteStatement>(state->statements.get(s));
	statement->append(exp);
	return s;
}

int addWhile(int e, int seq)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto sequence = state->statementSeqs.get(seq);
	auto statement = make_shared<WhileStatement>(exp, sequence);
	return state->statements.add(statement);
}


///// EXPRESSION SECTION //////
int addAdd(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<AddExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addAnd(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<AndExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addChr(int e)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(e);
	auto exp = make_shared<ChrExpression>(exp1);
	return state->expressions.add(exp);
}

int addDivide(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<DivideExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addEquals(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<EqualsExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addGreaterOrEqual(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<GreaterOrEqualExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addGreaterThan(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<GreaterThanExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addLessOrEqual(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<LessOrEqualExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addLessThan(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<LessThanExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addMod(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<ModExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addMult(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<MultExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addNeg(int e)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(e);
	auto exp = make_shared<NegExpression>(exp1);
	return state->expressions.add(exp);
}	

int addNot(int e)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(e);
	auto exp = make_shared<NotExpression>(exp1);
	return state->expressions.add(exp);
}

int addNotEqual(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<NotEqualExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addOr(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<OrExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addOrd(int e)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(e);
	auto exp = make_shared<OrdExpression>(exp1);
	return state->expressions.add(exp);
}

int addPred(int e)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(e);
	auto exp = make_shared<PredExpression>(exp1);
	return state->expressions.add(exp);
}

int addSub(int f, int s)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(f);
	auto exp2 = state->expressions.get(s);
	auto exp = make_shared<SubExpression>(exp1, exp2);
	return state->expressions.add(exp);
}

int addSucc(int e)
{
	auto state = State::instance();
	auto exp1 = state->expressions.get(e);
	auto exp = make_shared<SuccExpression>(exp1);
	return state->expressions.add(exp);
}


int addInteger(int i)
{
	auto state = State::instance();
	auto exp = make_shared<LiteralExpression>(i);
	return state->expressions.add(exp);
}

int addCharLit(char c)
{
	auto state = State::instance();
	auto exp = make_shared<LiteralExpression>(c);
	return state->expressions.add(exp);
}

int addString(char* s)
{
	auto state = State::instance();
	auto index = stringTable.insert(s);
	auto exp = make_shared<StringExpression>(index);
	delete (s);
	return state->expressions.add(exp);
}


///// VARIABLE AND LVAL SECTION //////
/*
Gets the location of a variable and turns it into an expression
*/
int loadLVal(int id)
{
	auto state = State::instance();
	auto lval = state->lvals.get(id);
	auto exp = make_shared<LoadExpression>(lval);
	return state->expressions.add(exp);
}	

/*
Get an integer for the current type index
*/
int lookupType(char* identifier)
{
	auto state = State::instance();
	auto table = state->getSymbolTable();
	auto type = table->lookupType(identifier);
	// Clean up memory
	delete (identifier);
	return state->typeList.add(type);
}

void addType(char* identifier, int typeIndex)
{
	auto state = State::instance();
	auto table = state->getSymbolTable();
	auto type = state->typeList.get(typeIndex);
	table->addType(identifier, type);
	delete (identifier);
}

/*
Adds an identifier to a list of identifiers
Second parameter is a character array since bison uses c not c++
Returns the listIndex of the list
*/
int addIdent(int listIndex, char* identifier)
{
	auto state = State::instance();
	if (listIndex == -1)
	{
		// Create a list to be added to
		auto list = make_shared<vector<string>>();
		list->push_back(identifier);
		return state->idLists.add(list);
	}
	else
	{
		// Add item to the end of the list of identifiers
		auto list = state->idLists.get(listIndex);
		list->push_back(identifier);
		return listIndex;
	}
}

int addIdent(int listIndex, std::string identifier)
{
	auto state = State::instance();
	if (listIndex == -1)
	{
		// Create a list to be added to
		auto list = make_shared<vector<string>>();
		list->push_back(identifier);
		return state->idLists.add(list);
	}
	else
	{
		// Add item to the end of the list of identifiers
		auto list = state->idLists.get(listIndex);
		list->push_back(identifier);
		return listIndex;
	}
}

/*
Adds an identifier to a list of identifiers
Second parameter is a character array since bison uses c not c++
Returns the listIndex of the list
*/
int addLVal(char* identifier)
{
	auto state = State::instance();
	auto lval = make_shared<IdAccess>(identifier, state->getSymbolTable()); 
	delete (identifier);
	return state->lvals.add(lval);
}

int addMemberAccess(int lv, char* identifier)
{
	cout << "Adding member access" << endl;
	auto state = State::instance();
	auto base = state->lvals.get(lv);
	auto lval = make_shared<MemberAccess>(base, identifier, state->getSymbolTable());
	delete (identifier);

	return state->lvals.add(lval);
}

int addArrayAccess(int lv, int e)
{
	cout << "Adding array access" << endl;
	auto state = State::instance();
	auto base = state->lvals.get(lv);
	auto exp = state->expressions.get(e);
	auto lval = make_shared<ArrayAccess>(base, exp, state->getSymbolTable());
	return state->lvals.add(lval);	
}

/*
Adds a constant to the list
*/
void addConst(char* identifier, int e)
{
	auto state = State::instance();
	auto exp = state->expressions.get(e);
	auto table = state->getSymbolTable();
	table->addConst(identifier, exp);
	delete (identifier);
}

/*
Add the variables to the symbol table
*/
void addVariables(int listIndex, int typeIndex)
{
	// Get the current state we are in
	auto state = State::instance();
	// Get the type from list of types
	auto type = state->typeList.get(typeIndex);
	// get the list of ids from the state
	auto idList = state->idLists.get(listIndex);
	for (auto && id : *idList)
	{
		state->getSymbolTable()->addVariable(id, type);
	}
}

/*
Add integer variables to the symbol table
*/
void addVariables(int listIndex, shared_ptr<Type> type)
{
	auto state = State::instance();
	auto idList = state->idLists.get(listIndex);
	for (auto && id : *idList)
	{
		state->getSymbolTable()->addVariable(id, type);
	}
}

} // End namespace
