#ifndef TYPE_HPP
#define TYPE_HPP

#include <string>
#include <memory>
#include <map>
using namespace std;

namespace Clegg5300
{

class Type
{
	public:
		virtual int getSize() = 0;
		virtual std::string name() = 0;
		virtual ~Type() = default;
};

/*
Define the integer base type
*/
class IntType : public Type
{
	int getSize() { return 4; }
	std::string name() { return "int"; }
};

/*
Define the character base type
*/
class CharType : public Type
{
	int getSize() { return 4; }
	std::string name() { return "char"; }
};

/*
Define the boolean base type
*/
class BoolType : public Type
{
	int getSize() { return 4; }
	std::string name() { return "bool"; }
};

/*
Define the string base type
*/
class StringType : public Type
{
	int getSize() { return 0; }
	std::string name() { return "string"; }
};

/*
Precreate all the base types
*/
class BuiltInType
{
	public:
	/*
	Create a shared pointer to integer type
	*/
	static std::shared_ptr<Type> getInt()
	{
		if (!memInt) memInt = make_shared<IntType>();
		return memInt;
	}
	/*
	Create a shared pointer to charater type
	*/
	static std::shared_ptr<Type> getChar()
	{
		if (!memChar) memChar = make_shared<CharType>();
		return memChar;
	}
	/*
	Create a shared pointer to bool type
	*/
	static std::shared_ptr<Type> getBool()
	{
		if (!memBool) memBool = make_shared<BoolType>();
		return memBool;
	}
	/*
	Create a shared pointer to string types
	*/
	static std::shared_ptr<Type> getString()
	{
		if (!memString) memString = make_shared<StringType>();
		return memString;
	}
	private:
	static std::shared_ptr<Type> memInt;
	static std::shared_ptr<Type> memChar;
	static std::shared_ptr<Type> memBool;
	static std::shared_ptr<Type> memString;
};

class ArrayType : public Type
{
	public:
		ArrayType(std::shared_ptr<Type> t, int s, int e) :
		Type(), type(t), startingIndex(s), endIndex(e) {}
		int getSize();
		std::string name()
		{
			return "Array";
		}
		int getStartIndex() { return startingIndex; }
		int getEndIndex() { return endIndex; }
		std::shared_ptr<Type> getType() { return type; }
	private:
		std::shared_ptr<Type> type;
		int startingIndex;
		int endIndex;
};

class RecordType : public Type
{
	public:
		RecordType() : Type() {}
		int getSize();
		std::string name()
		{
			return "Record";
		}
		void addMember(std::string, std::shared_ptr<Type>);
		std::shared_ptr<Type> getType(std::string) const;
		int getOffset(std::string) const;
		std::map<std::string, std::shared_ptr<Type>> getFields() { return fields; }
	private:
		int offset;
		std::map<std::string, std::shared_ptr<Type>> fields;
		std::map<std::string, int> fieldOffset;
};

class ReferenceType : public Type
{
	public:
		ReferenceType(std::shared_ptr<Type> t) : Type(), type(t) {}
		virtual int getSize() override final { return 4; }
		virtual std::string name() override final
		{
			return "Reference " + type->name();
		}
		virtual ~ReferenceType() = default;
		std::shared_ptr<Type> getType() { return type; }
	private:
		std::shared_ptr<Type> type;
};
}

#endif
