#ifndef STRING_TABLE_HPP
#define STRING_TABLE_HPP

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

extern std::ofstream oFile;

namespace Clegg5300
{

class StringTable
{
	public:
		void printList();
		int insert(string);
	private:
		std::vector<string> stringList;
		int currentNumber = 0;
};

}
#endif
