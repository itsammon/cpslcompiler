#ifndef LVAL_HPP
#define LVAL_HPP

#include "Register.hpp"
#include "SymbolTable.hpp"
#include "MemoryAccessExpression.hpp"

#include <memory>
#include <string>

namespace Clegg5300
{
class LVal
{
	public:
		LVal(std::shared_ptr<SymbolTable> t) : table(t) {}
		~LVal() = default;
		virtual std::unique_ptr<Register> toRegister()=0;
		virtual std::shared_ptr<Type> getType()=0;
		virtual bool isConst()=0;
		virtual int getValue()=0;
		virtual shared_ptr<Expression> getAddress()=0;
		virtual int getOffset()=0;
		virtual MemoryLocation getMemLocation()=0;
		virtual string getLocation(int)=0;
	protected:
		std::shared_ptr<SymbolTable> table;
};

class IdAccess : public LVal
{
	public:
		IdAccess(std::string id, std::shared_ptr<SymbolTable> t) : LVal(t), id(id) {}
		std::unique_ptr<Register> toRegister();
		bool isConst();
		std::string getLocation(int = 0);
		shared_ptr<Type> getType();
		int getValue();
		int getOffset();
		MemoryLocation getMemLocation();
		shared_ptr<Expression> getAddress();
	private:
		string id;
};

class MemberAccess : public LVal
{
	public:
		MemberAccess(std::shared_ptr<LVal> b, std::string f, std::shared_ptr<SymbolTable> t) : LVal(t), base(b), field(f), type() {}
		std::unique_ptr<Register> toRegister();
		bool isConst() { return false; }
		shared_ptr<Type> getType()
		{
			if (!type)
			{
				type = getBType()->getType(field);
			}
			return type;
		}
		int getValue() { return 0; }
		std::shared_ptr<RecordType> getBType() const
		{
			cout << "Getting base type" << endl;
			auto rtype = std::dynamic_pointer_cast<ReferenceType>(base->getType());
			if (rtype) return std::dynamic_pointer_cast<RecordType>(rtype->getType());
			return std::dynamic_pointer_cast<RecordType>(base->getType());
		}
		int getOffset();
		std::string getLocation(int = 0);
		MemoryLocation getMemLocation() { return base->getMemLocation(); }
		shared_ptr<Expression> getAddress();
	private:
		std::shared_ptr<LVal> base;
		std::string field;
		mutable std::shared_ptr<Type> type;
};

class ArrayAccess : public LVal
{
	public:
		ArrayAccess(std::shared_ptr<LVal> b, std::shared_ptr<Expression> e, std::shared_ptr<SymbolTable> t) : LVal(t), base(b), exp(e) {}
		std::unique_ptr<Register> toRegister();
		bool isConst() { return false; }
		shared_ptr<Type> getType() { return getAType()->getType(); }
		int getValue() { exp->emit(); return exp->getValue(); }
		shared_ptr<ArrayType> getAType() const
		{
			// Get the array type
			auto rtype = std::dynamic_pointer_cast<ReferenceType>(base->getType());
			if (rtype)
			{
				auto type = std::dynamic_pointer_cast<ArrayType>(rtype->getType());
				return type;
			}
			
			auto type = std::dynamic_pointer_cast<ArrayType>(base->getType());
			if (!type) cout << "Error - " << base->getType()->name() << endl;
			return type;
		}
		int getOffset();
		std::string getLocation(int = 0);
		MemoryLocation getMemLocation() { return base->getMemLocation(); }
		virtual shared_ptr<Expression> getAddress();
	private:
		std::shared_ptr<LVal> base;
		std::shared_ptr<Expression> exp;

};

}
#endif
