#ifndef REGISTER_HPP
#define REGISTER_HPP

#include <memory>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

class Register
{
	public:
		static void init();
		static std::unique_ptr<Register> allocateRegister();
		static std::vector<std::string> getInUse() { return inUse; }
		~Register()
		{
			inUse.erase(std::remove(inUse.begin(), inUse.end(), name), inUse.end());
			pool.push_back(name);
		}
		friend std::ostream& operator<< (std::ostream&, Register const &);
	private:
		Register(std::string n): name(n) {}
		std::string name;
		static std::vector<std::string> pool;
		static std::vector<std::string> inUse;
};

#endif
