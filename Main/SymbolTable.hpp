#ifndef SYMBOL_TABLE_HPP
#define SYMBOL_TABLE_HPP

#include <memory>
#include <string>
#include <map>
#include "Symbol.hpp"
#include "Type.hpp"
#include "Expression.hpp"
using namespace std;

namespace Clegg5300
{
class SymbolTable
{
	public:
		SymbolTable(shared_ptr<SymbolTable> parent, MemoryLocation loc, int off = 0) :
			parent(parent), memLocation(loc), memoryOffset(off), parameterOffset(0) { returnLabel = "prog"; }
	
		shared_ptr<Type> lookupType(string id);
		shared_ptr<Symbol> lookupVariable(string id);
		shared_ptr<Expression> lookupConst(string id);
		void addType(string id, shared_ptr<Type> type);
		void addVariable(string id, shared_ptr<Type> type);
		// Parameter is a special type of variable
		void addParameter(string id, shared_ptr<Type> type); 
		void addConst(string id, shared_ptr<Expression> constant);
		int getMemOffset() { return memoryOffset; }
		MemoryLocation getMemLocation() { return memLocation; }
		shared_ptr<SymbolTable> getParent() { return parent; }

		void setReturn(std::string label) { returnLabel = label; }
		std::string getLabel() { return returnLabel; }

	private:
		std::map<std::string, std::shared_ptr<Symbol>> variables;
		std::map<std::string, std::shared_ptr<Type>> types;
		std::map<std::string, std::shared_ptr<Expression>> consts;
		int memoryOffset;
		int parameterOffset;
		std::shared_ptr<SymbolTable> parent;
		MemoryLocation memLocation;
		std::string returnLabel;
};
}
#endif
