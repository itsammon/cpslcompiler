#include "Type.hpp"

#include <iostream>
using namespace std;
// Declarations to allow linking
std::shared_ptr<Clegg5300::Type> Clegg5300::BuiltInType::memInt;
std::shared_ptr<Clegg5300::Type> Clegg5300::BuiltInType::memBool;
std::shared_ptr<Clegg5300::Type> Clegg5300::BuiltInType::memChar;
std::shared_ptr<Clegg5300::Type> Clegg5300::BuiltInType::memString;

namespace Clegg5300
{

int ArrayType::getSize()
{
	auto size = type->getSize();
	auto numElements = (endIndex+1) - startingIndex;
	return size * numElements;
}

int RecordType::getSize()
{
	int size = 0;
	for (auto &iter : fields)
	{
		size += iter.second->getSize();
	}
	return size;
}

void RecordType::addMember(std::string id, std::shared_ptr<Type> type)
{
	cout << "Adding member " << id << " of type " << type->name() << endl;
	
	fields[id] = type;
	fieldOffset[id] = offset;
	offset += type->getSize();
}

std::shared_ptr<Type> RecordType::getType(std::string id) const
{
	auto element = fields.find(id);
	if (element == fields.end())
	{
		cout << "Unable to find type for member field " << id << endl;
		return std::shared_ptr<Type>();
	}
	
	// Return the type for the field
	return element->second;
}

int RecordType::getOffset(std::string id) const
{
	auto element = fieldOffset.find(id);
	if (element == fieldOffset.end())
	{
		cout << "Unable to find offset for member field " << id << endl;
		return -1;
	}
	
	// Return the offset
	return element->second;
}

}
